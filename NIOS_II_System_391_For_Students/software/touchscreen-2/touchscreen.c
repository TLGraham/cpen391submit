/*
                                -----------
                           --|1            2|--
                           --|3            4|--
                           --|5            6|--
                           --|7   GPIO 0   8|--
                           --|9    JP1    10|--
V+ On Touch Screen -- VCC5 --|11          12|-- GND --- Gnd on Touch Screen
RX On Touch Screen --   Tx --|13          14|-- Rx  --- TX
                           --|15          16|--


*/


#include <stdio.h>

#define touch_Control (*(volatile unsigned char *)(0x84000230))
#define touch_Status  (*(volatile unsigned char *)(0x84000230))
#define touch_TxData  (*(volatile unsigned char *)(0x84000232))
#define touch_RxData  (*(volatile unsigned char *)(0x84000232))
#define touch_Baud    (*(volatile unsigned char *)(0x84000234))

#define   size                  0x01
// set 9600 baud
#define   set_baud              0x07
#define   enable_touchscreen    0x12
#define   disable_touchscreen   0x13
// 8 data bits and 1 stop bit
#define   data_bit_stop_bit     0x15
#define   synchronize           0x55
#define   pen_up                0x80
#define   pen_down              0x81

// A data type to hold a point/ coord/*{{{*/

typedef struct {
  unsigned int x;
  unsigned int y;
} Point;

/*}}}*/
// Enum used to select what action to take when calling GetPressOrRelease/*{{{*/

typedef enum {
  PRESS, RELEASE
} choice;

/*}}}*/
// wait for x thousand loops/*{{{*/

void wait_xk_loops(unsigned int x) {
  unsigned int i;
  x *= 1000;
  for (i=0; i < x; i++);
}

/*}}}*/
// Initialize touch screen Controller./*{{{*/

void Init_Touch(void) {
  touch_Control = data_bit_stop_bit;

  touch_Baud = set_baud;

  printf("Initializing Touch Screen...\n");
  wait_xk_loops(70);

  // Synchronize
  touch_TxData = synchronize;
  wait_xk_loops(70);
  // size
  touch_TxData = size;
  wait_xk_loops(70);
  // Command
  touch_TxData = disable_touchscreen;

  // Synchronize
  touch_TxData = synchronize;
  wait_xk_loops(70);
  // size
  touch_TxData = size;
  wait_xk_loops(70);
  // Command
  touch_TxData = enable_touchscreen;
  wait_xk_loops(70);

  printf("Touch Screen Initialized!\n");

}

/*}}}*/
// Test if screen touched regardless of event type./*{{{*/

int ScreenTouched(void) {
  return ((touch_Status & 0x01) == 0x01);
}

/*}}}*/
// Wait for screen to be touched./*{{{*/

void WaitForTouch(void) {
  while (!ScreenTouched());
}

/*}}}*/
// This is a helper function for the GetPress and GetRelease functions*{{{*/

Point GetPressOrRelease(choice c) {
  Point p1;
  char data1;
  char data2;
  char data3;
  char data4;

  if ( c == PRESS ) {
    // Wait for the touchscreen to be touched
    while (touch_RxData != pen_down);
    // Wait for that value to go away.
    while (touch_RxData == pen_down);
  }
  else {
    // Wait for the touchscreen to be touched
    while (touch_RxData != pen_up);
    // Wait for that value to go away.
    while (touch_RxData == pen_up);
  }

  data1 = touch_RxData;
  while (touch_RxData == data1);
  data2 = touch_RxData;
  while (touch_RxData == data2);
  data3 = touch_RxData;
  while (touch_RxData == data3);
  data4 = touch_RxData;

  // Format p1->x to be x11 x10 x9 ... x1 x0
  p1.x = (data2 << 7) + data1;
  p1.y = (data4 << 7) + data3;

  // Calibrated correctly so that it maps to a pixel on screen.
  p1.y /= 8.549;
  p1.x /= 5.125;

  printf("press p1.x = %d\n", p1.x);
  printf("press p1.y = %d\n", p1.y);

  return p1;
}

/*}}}*/
// This function waits for a touch screen press event and returns X, Y coord./*{{{*/

Point GetPress(void) {
  return GetPressOrRelease(PRESS);
}

/*}}}*/
// This function waits for a touch screen release event and returns X,Y coord/*{{{*/

Point GetRelease(void) {
  return GetPressOrRelease(RELEASE);
}

/*}}}*/

int main() {
  Init_Touch();
  while (1) {
    GetPress();
  }
  return 0;
}
