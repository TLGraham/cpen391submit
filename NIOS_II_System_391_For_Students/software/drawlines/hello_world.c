/*
                                -----------
                           --|1            2|--
                           --|3            4|--
                           --|5            6|--
                           --|7   GPIO 0   8|--
       Purple              --|9    JP1    10|--          White
V+ On Touch Screen -- VCC5 --|11          12|-- GND --- Gnd on Touch Screen
RX On Touch Screen --   Tx --|13          14|-- Rx  --- TX
       Grey                --|15          16|--      Black


*/


#include <stdio.h>
#include "Fonts.h"
#include <string.h>
#include <stdlib.h>
#include "Drawlines.h"
#include "TouchScreen.h"
#include "GPS.h"



#define CHAR_WIDTH 12
#define CHAR_HEIGHT 14
#define XRES 800
#define YRES 480



//Keyboard constants
#define NUMLETTERS                 26
#define BUTTON_H                   60
#define BUTTON_W                   60
#define FIRST_LETTER_ROW_CUTOFF    9
#define SECOND_LETTER_ROW_CUTOFF   18

#define NUMPUNCTNUM 25
#define FIRST_PUNCTNUM_ROW_CUTOFF  9
#define SECOND_PUNCTNUM_ROW_CUTOFF 18

#define KEY_COLOUR WHITE
#define KEYBOARD_BACKGROUND_COLOUR BLACK

#define MAX_STRING 64


typedef struct {
  int x_start;
  int y_start;
  int width;
  int height;
  int colour;
  int id;
  char message[MAX_STRING];   // Button text on first line
  char message2[MAX_STRING];  // Button text on second line (used only for
                              // drawing a two line button)
} button;

typedef struct {
  button box;
  int cursor_pos;                       // Used for backspacing
  bool isTextboxOn;                     // Turns on and off the textbox
  char last_saved_string[MAX_STRING];   // holds the string that was saved with
                                        // an enter key
} textbox;


bool isButtonPressed(Point p, button b) {
  if (p.x >= b.x_start && p.x <= b.x_start+b.width &&
      p.y >= b.y_start && p.y <= b.y_start+b.height) {

#ifdef DEBUG
    printf("button id #%d Pressed\n", b.id);
#endif // DEBUG
    return true;
  }
  return false;

}

void OutGraphicsCharFont1(int x, int y, int fontcolour, int backgroundcolour,
                          int c, bool erase) {

  register int row, column, theX = x, theY = y;
  register int pixels;
  register char theColour = fontcolour;
  register int BitMask, theC = c;

  // if x, y coord are off screen, dont bother
  if (((short)(x) > (short)(XRES-1)) || ((short)(y) > (short)(YRES-1))) {
    return;
  }

  // if printable character subtract hex 20
  if (((short)(theC) >= (short)(' ')) && ((short)(theC) <= (short)('~'))) {
    theC = theC - 0x20;

    for (row = 0; (char)(row) < (char)(14); row++) {
      pixels = Font10x14[theC][row];
      BitMask = 0x200;

      for (column = 0; (char)(column) < (char)(10); column++) {
        if ((pixels & BitMask)) {
          WriteAPixel(theX+column, theY+row, theColour);
        }
        else {
          if (erase == true) {
            WriteAPixel(theX+column, theY+row, backgroundcolour);
          }
        }
        BitMask = BitMask >> 1;
      }
    }
  }
}

void printString(char* string, int x_start, int y_start,
                 int char_colour, int background_colour) {
  int i;
  int x = x_start;
  int y = y_start;
  const int char_width = 10;

  for (i = 0; i < strlen(string); i++) {
#ifdef DEBUG
    printf("string[%d] = %c\n", i, string[i]);
#endif // DEBUG
    OutGraphicsCharFont1(x, y, char_colour, background_colour, string[i], false);
    x = x + char_width + 2;
  }
}

// Draws a button with the text centered on one line
void drawButton(button b) {
  FramedRectangle(b.x_start, b.y_start, b.width, b.height, BLACK, b.colour);

  int x_string = b.x_start + ((b.width - CHAR_WIDTH*strlen(b.message))/2);
  int y_string = b.y_start + ((b.height - CHAR_HEIGHT)/2);
  printString(b.message, x_string, y_string, BLACK, b.colour);
}

// Draws the textbox with text.
void drawTextbox(textbox t) {
  if (t.isTextboxOn) {
    drawButton(t.box);
  }
}

// Draws a button with the button text centered on two lines
void drawButton_two_lines(button b) {
  FramedRectangle(b.x_start, b.y_start, b.width, b.height, BLACK, b.colour);

  int x_string = b.x_start + ((b.width - CHAR_WIDTH*strlen(b.message))/2);
  int y_string = b.y_start + ((b.height - CHAR_HEIGHT*2+2)/2);
  printString(b.message, x_string, y_string, BLACK, b.colour);

  x_string = b.x_start + ((b.width - CHAR_WIDTH*strlen(b.message2))/2);
  y_string += CHAR_HEIGHT+2;
  printString(b.message2, x_string, y_string, BLACK, b.colour);
}

char* upperletters_str[NUMLETTERS] = {"Q","W","E","R","T","Y","U","I","O","P",
                                        "A","S","D","F","G","H","J","K","L",
                                           "Z","X","C","V","B","N","M"};

char* lowerletters_str[NUMLETTERS] = {"q","w","e","r","t","y","u","i","o","p",
                                        "a","s","d","f","g","h","j","k","l",
                                           "z","x","c","v","b","n","m"};

char* num_punc_str[NUMPUNCTNUM] = {"1","2","3","4","5","6","7","8","9","0",
                                     "-","/",":",";","(",")","$","&","@",
                                        "\"",".",",","?","!","\'"};

// we can use this to check when a button is pressed...
// ie if (isButtonPressed(pressPoint, k.letters[A]))
typedef enum { Q,W,E,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M } alpha;

typedef struct {
  button letters[NUMLETTERS];             // Letter buttons
  button numbers_and_punct[NUMPUNCTNUM];  // number and punctuation buttons
  button enter;
  button space;
  button shift;
  button backspace;
  button number_letter_toggle;
  bool isCapsOn;                          // When true, the capital letter keys
                                          // are drawn when drawKeyboard is called

  bool isNumbersOn;                       // When true, the number and
                                          // punctuation keys are drawn when
                                          // drawKeyboard is called

  bool isKeyboardOn;                      // When true, the keyboard is drawn
                                          // when drawKeyboard is called
} keyboard;

void drawKeyboard(keyboard *k) {
  if (k->isKeyboardOn) {
    int i;
    if (k->isNumbersOn) {
      // Draw numbers
      for (i = 0; i < NUMPUNCTNUM; i++) {
        drawButton(k->numbers_and_punct[i]);
      }
      strcpy(k->number_letter_toggle.message, "abc");
      drawButton(k->number_letter_toggle);
    }
    else if (k->isCapsOn) {
      // draw Capital Letters
      for (i = 0; i < NUMLETTERS; i++) {
        strcpy(k->letters[i].message, upperletters_str[i]);
        drawButton(k->letters[i]);
      }
      strcpy(k->number_letter_toggle.message, "123");
      drawButton(k->number_letter_toggle);
    }
    else {
      // draw lowercase letters;
      for (i = 0; i < NUMLETTERS; i++) {
        strcpy(k->letters[i].message, lowerletters_str[i]);
        drawButton(k->letters[i]);
      }
      strcpy(k->number_letter_toggle.message, "123");
      drawButton(k->number_letter_toggle);
    }

    drawButton(k->space);
    drawButton(k->enter);
    drawButton(k->shift);
    drawButton(k->backspace);
  }
}

textbox initializeTextbox() {
  textbox t;

  t.cursor_pos = 0;
  t.isTextboxOn = 0;

  t.box.x_start = 100;
  t.box.y_start = 140;
  t.box.width = 500;
  t.box.height = 30;
  t.box.colour = WHITE;
  t.box.id = 2;
  strcpy(t.box.message, "");

  return t;
}

keyboard initializeKeyboard() {
  keyboard k;
  k.isCapsOn = 0;
  k.isNumbersOn = 0;
  k.isKeyboardOn = 0;

  int i;
  //int rownum = 1;
  int button_x = 55;
  int button_y = 175;

  // The buttons for the letters. Note that the same buttons are used for
  // capital and lowercase letters... What they buttons print and display is up
  // to the state that the isCapsOn variable is in when the drawKeyboard
  // function is called.
  for (i = 0; i < NUMLETTERS; i++) {
    k.letters[i].x_start = button_x;
    k.letters[i].y_start = button_y;
    k.letters[i].width = BUTTON_W;
    k.letters[i].height = BUTTON_H;
    k.letters[i].colour = WHITE;
    k.letters[i].id = i;

    if (i == FIRST_LETTER_ROW_CUTOFF) {
      button_y += BUTTON_H + 10;
      button_x =  90;
    }
    else if ( i == SECOND_LETTER_ROW_CUTOFF ) {
      button_x = 165;
      button_y += BUTTON_H + 10;
    }
    else
      button_x += BUTTON_W + 10;
  }

  button_x = 55;
  button_y = 175;

  // The number and punctuation buttons
  for (i = 0; i < NUMPUNCTNUM; i++) {
    k.numbers_and_punct[i].x_start = button_x;
    k.numbers_and_punct[i].y_start = button_y;
    k.numbers_and_punct[i].width = BUTTON_W;
    k.numbers_and_punct[i].height = BUTTON_H;
    k.numbers_and_punct[i].colour = WHITE;
    k.numbers_and_punct[i].id = i;
    strcpy(k.numbers_and_punct[i].message, num_punc_str[i]);

    if (i == FIRST_PUNCTNUM_ROW_CUTOFF) {
      button_y += BUTTON_H + 10;
      button_x =  90;
    }
    else if ( i == SECOND_PUNCTNUM_ROW_CUTOFF ) {
      button_x = 165;
      button_y += BUTTON_H + 10;
    }
    else
      button_x += BUTTON_W + 10;
  }

  // The button that toggles whether we are looking at letters or numbers and
  // punctuation (They are the same button, but depending on the state will
  // display either "abc" or "123")
  k.number_letter_toggle.x_start = 125;
  k.number_letter_toggle.y_start = button_y;
  k.number_letter_toggle.width = BUTTON_W;
  k.number_letter_toggle.height = BUTTON_H;
  k.number_letter_toggle.colour = WHITE;
  k.number_letter_toggle.id = 26;

  k.space.x_start = 210;
  k.space.y_start = button_y + 10;
  k.space.width = 150;
  k.space.height = BUTTON_H;
  k.space.colour = WHITE;
  k.space.id = 27;
  strcpy(k.space.message, "Space");

  k.enter.x_start = 370;
  k.enter.y_start = button_y + 10;
  k.enter.width = 90;
  k.enter.height = BUTTON_H;
  k.enter.colour = WHITE;
  k.enter.id = 28;
  strcpy(k.enter.message, "Enter");

  k.shift.x_start = 470;
  k.shift.y_start = button_y + 10;
  k.shift.width = 90;
  k.shift.height = BUTTON_H;
  k.shift.colour = WHITE;
  k.shift.id = 28;
  strcpy(k.shift.message, "Shift");

  k.backspace.x_start = 570;
  k.backspace.y_start = button_y + 10;
  k.backspace.width = 50;
  k.backspace.height = BUTTON_H;
  k.backspace.colour = WHITE;
  k.backspace.id = 28;
  strcpy(k.backspace.message, "BS");

  return k;
}

// Function returns 1 of there is something to read from t.last_saved_string.
// Otherwise, this returns 0.
int keyboardListener(Point p, keyboard *k, textbox *t) {
  // Enter key saves the string that is entered in t.last_saved_string and
  // resets t.box.message to nothing. Also closes the keyboard
  if (isButtonPressed(p, k->enter)) {
    k->isKeyboardOn = 0;
    t->isTextboxOn = 0;
    strcpy(t->last_saved_string, t->box.message);
    strcpy(t->box.message, "");
    return 1;
  }
  // Backspace moves the cursor back, and sets the character at the new cursor
  // position to be a null terminator.
  if (isButtonPressed(p, k->backspace)) {
    t->cursor_pos--;
    if (t->cursor_pos < 0) {
      t->cursor_pos = 0;
    }
    k->isCapsOn = 0;

    t->box.message[t->cursor_pos] = '\0';
    return 0;
  }
  else if (isButtonPressed(p, k->space)) {
    strcat(t->box.message, " ");
    t->cursor_pos++;
    k->isCapsOn = 0;
    return 0;
  }
  else if (isButtonPressed(p, k->number_letter_toggle)) {
    if (k->isNumbersOn == 1) {
      k->isNumbersOn = 0;
    }
    else {
      k->isNumbersOn = 1;
    }
    k->isCapsOn = 0;
    return 0;
  }
  else if (isButtonPressed(p, k->shift)) {
    k->isCapsOn = 1;
    return 0;
  }
  else if (k->isNumbersOn) {
    int i;
	//Remove m button
	FramedRectangle(405,255,BUTTON_H,BUTTON_W,BLACK,BLACK);
    for (i = 0; i < NUMPUNCTNUM; i++) {
      if (isButtonPressed(p, k->numbers_and_punct[i])) {
        strcat(t->box.message, num_punc_str[i]);
        t->cursor_pos++;
        k->isCapsOn = 0;
        return 0;
      }
    }
  }
  else {
    int i;
    for (i = 0; i < NUMLETTERS; i++) {
      if (isButtonPressed(p, k->letters[i])) {
        if (k->isCapsOn) {
          strcat(t->box.message, upperletters_str[i]);
        }
        else {
          strcat(t->box.message, lowerletters_str[i]);
        }
        t->cursor_pos++;
        k->isCapsOn = 0;
        return 0;
      }
    }
  }
  return 0;
}


int main() {
  Point p1;

  Init_Touch();
  ClearScreen(BLACK);

  // Toggle is used to toggle the keyboard on and off.
  button toggle;

  toggle.x_start = 550;
  toggle.y_start = 40;
  toggle.width = 250;
  toggle.height = 80;
  toggle.colour = RED;
  toggle.id = 1;
  strcpy(toggle.message, "Keyboard");
  strcpy(toggle.message2, "Toggle");

  drawButton_two_lines(toggle);

  keyboard k = initializeKeyboard();
  textbox t = initializeTextbox();

  // Toggle from one menu to another by pressing the new_location button.
  while (1) {
  p1 = GetRelease();
  if (isButtonPressed(p1, toggle)) {
    if (k.isKeyboardOn == 1) {
      k.isKeyboardOn = 0;
      t.isTextboxOn = 0;
    }
    else {
      k.isKeyboardOn = 1;
      t.isTextboxOn = 1;
    }
  }
  else if (k.isKeyboardOn) {
    int isThereSomethingToRead = 0;
    // checks if the touch was on a keyboard key. If it was, it preforms the
    // action of that key.
    isThereSomethingToRead = keyboardListener(p1, &k, &t);

    if (isThereSomethingToRead) {
      printf("t.last_string contains string: %s\n", t.last_saved_string);
    }
  }

     if(!k.isKeyboardOn){
    	 ClearScreen(BLACK);
    	 drawButton_two_lines(toggle);
     }
     else{
    	 drawButton_two_lines(toggle);
    	 drawKeyboard(&k);
    	 drawTextbox(t);
     }

    wait_xk_loops(200);
  }

  return 0;

}
