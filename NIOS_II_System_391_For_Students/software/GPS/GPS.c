#include "GPS.h"

void main(void)
{

    Init_GPS();
    int count;
    char* nmea_string = (char*)malloc(sizeof(char));
    read_one_data_entry (nmea_string);
    printf("%s\n", nmea_string);
    while(test_GPGGA(nmea_string) == FALSE){
        free(nmea_string);
        char* nmea_string = (char*)malloc(sizeof(char));
        read_one_data_entry (nmea_string);
        printf("%s\n", nmea_string);
    }
    GPS_info info = parse_GPS_packet (nmea_string);

    touchscreen_coor coord = relative_position(info);
    printf("The x coordinate is:%d and the y coordinate is:%d\n",coord.touchscreen_x,coord.touchscreen_y);

    alt_up_character_lcd_dev * char_lcd_dev;
    char_lcd_dev = alt_up_character_lcd_open_dev("/dev/character_lcd_0");
    if(char_lcd_dev == NULL)
        alt_printf("Error: could not open character LCD device\n");
    else {
        alt_printf("Address stored in ip variable: %x\n", push_buttons123 );
        printf("Value of *ip variable: %c\n", *push_buttons123 );
    }


    while(1){

        int number = listener();

        alt_up_character_lcd_init(char_lcd_dev);
        if( number == 2){
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,0,0);
            alt_up_character_lcd_string(char_lcd_dev, "The latitude is: ");
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,0,1);
            alt_up_character_lcd_string(char_lcd_dev, info.latitude);
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,10,1);
            if(info.N_S_indicator == 'N')
                alt_up_character_lcd_string(char_lcd_dev, "North");
            else
                alt_up_character_lcd_string(char_lcd_dev, "South");
        }
        else if ( number == 1 ){

            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,0,0);
            alt_up_character_lcd_string(char_lcd_dev, "The longitude is: ");
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,0,1);
            alt_up_character_lcd_string(char_lcd_dev, info.longitude);
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,11,1);
            if(info.E_W_indicator == 'E')
                alt_up_character_lcd_string(char_lcd_dev, "East");
            else
                alt_up_character_lcd_string(char_lcd_dev, "West");
        }
        else if (number == 0){

            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,0,0);
            alt_up_character_lcd_string(char_lcd_dev, "The time is: ");
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,0,1);
            alt_up_character_lcd_string(char_lcd_dev,info.hour);
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,2,1);
            alt_up_character_lcd_string(char_lcd_dev, ":");
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,3,1);
            alt_up_character_lcd_string(char_lcd_dev, info.minute);
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,5,1);
            alt_up_character_lcd_string(char_lcd_dev, ":");
            alt_up_character_lcd_set_cursor_pos(char_lcd_dev,6,1);
            alt_up_character_lcd_string(char_lcd_dev, info.seconds);
        }


    }

}
