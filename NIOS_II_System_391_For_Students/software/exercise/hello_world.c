/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>
#define RS232_Control  (*(volatile unsigned char *)(0x84000200))
#define RS232_Status   (*(volatile unsigned char *)(0x84000200))
#define RS232_TxData   (*(volatile unsigned char *)(0x84000202))
#define RS232_RxData   (*(volatile unsigned char *)(0x84000202))
#define RS232_Baud     (*(volatile unsigned char *)(0x84000204))

int main()
{
  printf("Hello from Nios II!\n");

  return 0;
}


void Init_RS232(void)
{
    // set up 6850 Control Register to utilise a divide by 16 clock
    // set RTS low, use 8 bits of data, no parity, 1 stop bit,
    // transmitter interrupt disabled
	RS232_Control = 0x55;
    
    // program baud rate generator to use 115k baud
	RS232_Baud = 0x01;
}

int putcharRS232(int c)
{
    // poll Tx bit in 6850 status register. Wait for it to become '1'
	while((RS232_Status & 0x02) != 0x02);
    // write 'c' to the 6850 TxData register to output the character
	RS232_TxData = c;
	return c;
}

int getcharRS232(void)
{
    // poll Rx bit in 6850 status register. Wait for it to become '1'
	while((RS232_Status & 0x01) != 0x01);
    // read received character from 6850 RxData register.
	char received_data = RS232_RxData;
	return received_data;
}

int RS232TestForReceiveData(void)
{
    // Test Rx bit in 6850 serial comms chip status register
    // if RX bit is set, return TRUE, otherwise return FALSE
	int set_flag = 0;  //false by default

	if((RS232_Status & 0x01) == 0x01){
		set_flag = 1;
	}

	return set_flag;
}
