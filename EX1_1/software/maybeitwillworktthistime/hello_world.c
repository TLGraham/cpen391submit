
#include <stdio.h>
#include <string.h>
#include "altera_up_avalon_character_lcd.h"

#define buttons (volatile char *) 0x0002000

int listener(void);

int main(void)
{
	alt_up_character_lcd_dev * char_lcd_dev;
	// open the Character LCD port
	char_lcd_dev = alt_up_character_lcd_open_dev ("/dev/character_lcd_0");


	if ( char_lcd_dev == NULL)
	alt_printf ("Error: could not open character LCD device\n");
	else {

		alt_printf("Address stored in ip variable: %x\n", buttons );
		printf("Value of *ip variable: %c\n", *buttons );
	}

	/* Initialize the character display */

	/* Write "Welcome to" in the first row */
	alt_up_character_lcd_init(char_lcd_dev);
	while(1){

		int number = listener();
		alt_up_character_lcd_init(char_lcd_dev);
		if( number == 3)
			alt_up_character_lcd_string(char_lcd_dev, "4");
		else if ( number == 2)
			alt_up_character_lcd_string(char_lcd_dev, "3");
		else if (number == 1)
			alt_up_character_lcd_string(char_lcd_dev, "2");
		else if (number == 0)
			alt_up_character_lcd_string(char_lcd_dev, "1");

	}

}

int listener(void){
	while(1){
		char byte = *buttons;
		int i;
		int temp;
		for(i = 3; 0 <= i; i--){
			temp = ((byte >> i) & 0x01);
			if(temp == 0)
				return i;
		}
	}

}


