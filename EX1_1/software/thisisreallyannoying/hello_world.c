
#include <stdio.h>
#include <string.h>
#include "altera_up_avalon_character_lcd.h"

#define buttons (volatile char *) 0x0002000

int main(void)
{
	alt_up_character_lcd_dev * char_lcd_dev;
	// open the Character LCD port
	char_lcd_dev = alt_up_character_lcd_open_dev ("/dev/character_lcd_0");


	if ( char_lcd_dev == NULL)
	alt_printf ("Error: could not open character LCD device\n");
	else {

		alt_printf("Address stored in ip variable: %x\n", buttons );
		printf("Value of *ip variable: %c\n", *buttons );
	}

	/* Initialize the character display */
	alt_up_character_lcd_init (char_lcd_dev);
	/* Write "Welcome to" in the first row */
	/* Write "the DE2 board" in the second row */
	char second_row[] = "the DE2 board\0";

	while(1){
		char byte = *buttons;
		int i;
		int button1 = (byte >> 3) & 0x01;
		int button2 = (byte >> 2) & 0x01;
		int button3 = (byte >> 1) & 0x01;
		int button4 = byte & 0x01;
		for(i = 3; 0 <= i; i --){
		 	alt_printf("Button 3's value is %d\n", button3*3);

		}

	}
	alt_up_character_lcd_set_cursor_pos(char_lcd_dev, 0, 1);
	alt_up_character_lcd_string(char_lcd_dev, second_row);
}



