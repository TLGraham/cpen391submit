/*
 * GPS.h
 *
 *  Created on: 2016-02-23
 *      Author: k8w9a
 */

#ifndef GPS_H_
#define GPS_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "altera_up_avalon_character_lcd.h"
#include <math.h>
#define push_buttons123 (volatile char *) 0x80001060

#define GPS_Control (*(volatile unsigned char *)(0x84000210))
#define GPS_Status  (*(volatile unsigned char *)(0x84000210))
#define GPS_TxData  (*(volatile unsigned char *)(0x84000212))
#define GPS_RxData  (*(volatile unsigned char *)(0x84000212))
#define GPS_Baud    (*(volatile unsigned char *)(0x84000214))
// set 9600 baud
#define   set_baud              0x07
#define   TRUE                  1
#define   FALSE                 0
#define   TOP_LEFT_CORNER_LAT   49.26899
#define   TOP_LEFT_CORNER_LON   123.261215
#define   LAT_RANGE             0.013563
#define   LONG_RANGE            0.024299
#define   WIDTH                 800.0
#define   HEIGHT                480.0


typedef struct {
    char hour[3];
    char minute[3];
    char seconds[3];
    int millisecond;
    char latitude[10];
    char N_S_indicator;
    char longitude[11];
    char E_W_indicator;

}GPS_info;

typedef struct {
    double latitude;
    double longitude;
}degree_coor;

typedef struct {
    int touchscreen_x;
    int touchscreen_y;
}touchscreen_coor;



int listener(void){
    while(1){
        char byte = *push_buttons123;

        int i;
        int temp;

        for(i = 2; 0 <= i; i--){
            temp = ((byte >> i) & 0x01);
            if( temp == 0 )
                return i;


        }
    }
}

// Initialize GPS Controller
void Init_GPS(void)
{
    // 8 data bits and 1 stop bit
    GPS_Control = 0x15;
    GPS_Baud = set_baud;
    printf("GPS Initialized!\n");

}

//Write Bytes to the GPS Chip
char GPS_Write (char command)
{
    //wait for the GPS Transimitted Register to be empty
    while ((GPS_Status & 0x02) != 0x02);
    //write bytes to the TX register
    GPS_TxData = command;

    return command;
}


//Read Bytes from the GPS Chip
char GPS_Read (void)
{
    //wait for the GPS Receive Data Register has something in it to be read
    while ((GPS_Status & 0x01) != 0x01);
    // read the data received
    char received_data = GPS_RxData;
    //printf("The char is: %c\n",received_data);

    return received_data;
}



// read one complete log entry
void read_one_data_entry (char* nmea_string){

    char end_char = '\0';
    *nmea_string = GPS_Read();
    char read_char = *nmea_string;

    if(read_char == '$'){
        read_char = GPS_Read();
        while (read_char!= 0x0a){
            *(nmea_string++) = read_char;
            read_char = GPS_Read();
        }
        *(nmea_string++) = 0x0a;
        *(nmea_string++) = end_char;
    }
    else{
        read_one_data_entry(nmea_string);
    }
}

GPS_info parse_GPS_packet (char* nmea_string){

    char *temp = nmea_string;
    char to_find = ',';
    GPS_info info;;
    // extract the time from hour to second
    temp = strchr(temp,to_find)+1;
    //info.hour = (*temp-'0')*10+(*(temp+1)-'0');
    strncpy(info.hour,temp,2);
    info.hour[2] = '\0';
    temp = temp+2;
    strncpy(info.minute,temp,2);
    info.minute[2] = '\0';
    temp = temp+2;
    strncpy(info.seconds,temp,2);
    info.seconds[2] = '\0';
    // extract millisecond
    temp = strchr(temp,'.')+1;
    info.millisecond = (*temp-'0')*100+(*(temp+1)-'0')*10+(*(temp+2)-'0');
    printf("the time is %s: %s: %s: %d\n",info.hour,info.minute,info.seconds,info.millisecond);

    //extract the latitude
    temp = strchr(temp,to_find)+1;
    if (*temp != to_find) {
        strncpy(info.latitude,temp,10);
        info.latitude[9]='\0';
        printf("latitude is: %s\n",info.latitude);
    }
    else{
        printf("latitude cannnot be determined\n");
    }

    //extract the N/S indicator
    temp = strchr(temp,to_find)+1;
    if (*temp != to_find) {
        info.N_S_indicator = *temp;
        printf("N/S indicator shows: %c\n", info.N_S_indicator);
    }
    else{
        printf("N/S indicator cannot be determined\n");
    }

    //extract the longitude
    temp = strchr(temp,to_find)+1;
    if (*temp != to_find) {
        strncpy(info.longitude,temp,11);
        info.longitude[10]='\0';
        printf("longitude is: %s\n",info.longitude);
    }
    else{
        printf("longitude cannnot be determined\n");
    }

    //extract the E/W indicator
    temp = strchr(temp,to_find)+1;
    if (*temp != to_find) {
        info.E_W_indicator = *temp;
        printf("E/W indicator shows: %c\n", info.E_W_indicator);
    }
    else{
        printf("E/W indicator cannot be determined\n");
    }

    return info;

}

int test_GPGGA (char *nmea_string){
    if (strstr(nmea_string, "GPGGA") == NULL) {
        return FALSE;
    }
    return TRUE;
}

// convert the ddmm.mmmm format coordinates to degree angle
degree_coor to_degree(GPS_info location){
    double temp_lat = atof(location.latitude);
    double temp_long = atof(location.longitude);
    degree_coor coordinates;

    int lat_degree = (int)(temp_lat/100.0);
    int lat_minute = (int)(temp_lat - lat_degree*100);
    double lat_seconds = (temp_lat- (double)lat_degree*100.0 - (double) lat_minute)*60.0;
    coordinates.latitude = (double)lat_degree + (double)lat_minute/60.0 + lat_seconds/3600.0;

    int long_degree = (int)(temp_long/100.0);
    int long_minute = (int)(temp_long - long_degree*100);
    double long_seconds = (temp_long- (double)long_degree*100.0 - (double) long_minute)*60.0;
    coordinates.longitude = (double)long_degree + (double)long_minute/60.0 + long_seconds/3600.0;

    if (location.E_W_indicator == 'W') {
        coordinates.longitude = coordinates.longitude * (-1.0);
    }

    if (location.N_S_indicator == 'S') {
        coordinates.latitude = coordinates.latitude *(-1.0);
    }

    return coordinates;
}

// calculate the GPS coordinate
touchscreen_coor relative_position(GPS_info current_coor){
    degree_coor coordinates = to_degree(current_coor);
    touchscreen_coor touchscreen_coordinates;

    touchscreen_coordinates.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+coordinates.longitude)/LONG_RANGE+0.4);
    touchscreen_coordinates.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-coordinates.latitude)/LAT_RANGE+0.4);

    return touchscreen_coordinates;
}


#endif /* GPS_H_ */
