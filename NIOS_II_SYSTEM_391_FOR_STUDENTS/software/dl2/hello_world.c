/*
-----------
--|1            2|--
--|3            4|--
--|5            6|--
--|7   GPIO 0   8|--
Purple              --|9    JP1    10|--          White
V+ On Touch Screen -- VCC5 --|11          12|-- GND --- Gnd on Touch Screen
RX On Touch Screen --   Tx --|13          14|-- Rx  --- TX
Grey                --|15          16|--      Black


*/

#include "GPS.h"
#include "Keyboard.h"

#define B_HEIGHT     40
#define B_WIDTH      40
#define B_COLOR      5 //YELLOW
#define MAX     64
#define B_SELECTED_COLOUR	7 //MAGENTA


// no building at beginning
int  BUILDING_EXIST=0;

typedef struct {
	char name[MAX];
	char building_usage[MAX];
	char course[MAX];
	char course_time[MAX];
	char course_date[MAX];
} building_info;

typedef struct {
	button box;		//physical representation of the building
	button ok;		//button to toggle the building on or off
	textbox info;	//displays user-entered information about class/building
	building_info msg;
} building;

building ubc_buildings[100];

void initialize_building_info (building_info initialize){
	strcpy(initialize.name, "Unavailable");
	strcpy(initialize.building_usage, "Unavailable");
	strcpy(initialize.course, "Unavailable");
	strcpy(initialize.course_time, "Unavailable");
	strcpy(initialize.course_date, "Unavailable");
}

building create_building (touchscreen_coor location){
	button box;
	building ubc_building;
	box.x_start = location.touchscreen_x;
	box.y_start = location.touchscreen_y;
	box.height  = B_HEIGHT;
	box.width = B_WIDTH;
	box.colour = B_COLOR;
	box.has_text = 0;
	drawButton(box);

	ubc_building.box = box;

	return ubc_building;
}

#include "sd.h"

int test_map (int numbers){
	GPS_info test_buildings[5];
	GPS_info math;
	math.E_W_indicator ='W';
	math.N_S_indicator ='N';
	strcpy(math.latitude , "4915.9773");
	strcpy(math.longitude , "12315.3178");
	test_buildings[0] = math;

	GPS_info sauder;
	sauder.E_W_indicator ='W';
	sauder.N_S_indicator ='N';
	strcpy(sauder.latitude , "4915.9157");
	strcpy(sauder.longitude, "12315.2200");
	test_buildings[1] = sauder;

	GPS_info chbe;
	chbe.E_W_indicator ='W';
	chbe.N_S_indicator ='N';
	strcpy(chbe.latitude , "4915.7506");
	strcpy(chbe.longitude, "12314.8312");
	test_buildings[2] = chbe;

	GPS_info totem;
	totem.E_W_indicator ='W';
	totem.N_S_indicator ='N';
	strcpy(totem.latitude , "4915.5814");
	strcpy(totem.longitude, "12315.2436");
	test_buildings[3] = totem;

	GPS_info hospital;
	hospital.E_W_indicator ='W';
	hospital.N_S_indicator ='N';
	strcpy(hospital.latitude,"4915.8497\0");
	strcpy(hospital.longitude, "12314.7500\0");
	test_buildings[4] = hospital;


	int count;
	for(count = 0; count<6; count++){
		touchscreen_coor onscreen = building_coord(test_buildings[count],B_HEIGHT,B_WIDTH);
		building build = create_building(onscreen);
		ubc_buildings[numbers+count]=build;

		if(count == 0){
			strcpy(ubc_buildings[count+numbers].msg.name,"MATHEMATICAL\0");
			strcpy(ubc_buildings[count+numbers].msg.building_usage, "Lectures\0");
		}
		else if(count == 1){
			strcpy(ubc_buildings[count+numbers].msg.name,"Henry Angus\0");
			strcpy(ubc_buildings[count+numbers].msg.building_usage, "Lectures\0");
		}

		else if(count == 2){
			strcpy(ubc_buildings[count+numbers].msg.name,"Chemical and Biological Engineering\0");
			strcpy(ubc_buildings[count+numbers].msg.building_usage, "Lectures\0");
		}

		else if(count == 3){
			strcpy(ubc_buildings[count+numbers].msg.name,"Totem Park\0");
			strcpy(ubc_buildings[count+numbers].msg.building_usage, "Student Residence\0");
		}

		else if(count == 4){
			strcpy(ubc_buildings[count+numbers].msg.name,"UBC Hospital\0");
			strcpy(ubc_buildings[count+numbers].msg.building_usage, "Hospital\0");
		}
		strcpy(ubc_buildings[count+numbers].msg.course, "Unavailable");
		strcpy(ubc_buildings[count+numbers].msg.course_date, "Unavailable");
		strcpy(ubc_buildings[count+numbers].msg.course_time, "Unavailable");
	}

	return numbers+5;
}



int main() {
	Point p1;


	Init_Touch();
	Init_GPS();


	ClearScreen(BLACK);

	// Toggle is used to toggle the keyboard on and off.
	button road;
	road.x_start = 675;
	road.y_start = 20;
	road.width = 125;
	road.height = 80;
	road.colour = RED;
	road.has_text = 1;
	road.valid = 1;
	strcpy(road.message, "Place");
	strcpy(road.message2, "Roads");

	button map;
	map.x_start = 675;
	map.y_start = 120;
	map.width = 125;
	map.height = 80;
	map.colour = RED;
	map.has_text = 1;
	map.valid = 1;
	strcpy(map.message, "Start");
	strcpy(map.message2, "Mapping");

	button load;
	load.x_start = 675;
	load.y_start = 220;
	load.width = 125;
	load.height = 80;
	load.colour = RED;
	load.has_text = 1;
	load.valid = 1;
	strcpy(load.message, "Load");
	strcpy(load.message2, "Buildings");

	button back;
	back.width = 100;
	back.height = 60;
	back.colour = YELLOW;
	back.has_text = 1;
	back.valid = 1;
	strcpy(back.message, "Back");

	button name;
	name.width = 100;
	name.height = 60;
	name.colour = CYAN;
	name.has_text = 1;
	name.valid = 1;
	strcpy(name.message, "NAME");

	button usage;
	usage.width = 100;
	usage.height = 60;
	usage.colour = CYAN;
	usage.has_text = 1;
	usage.valid = 1;
	strcpy(usage.message, "Usage");

	button c_code;
	c_code.width = 100;
	c_code.height = 60;
	c_code.colour = CYAN;
	c_code.has_text = 1;
	c_code.valid = 1;
	strcpy(c_code.message, "Code");

	button c_date;
	c_date.width = 100;
	c_date.height = 60;
	c_date.colour = CYAN;
	c_date.has_text = 1;
	c_date.valid = 1;
	strcpy(c_date.message, "Date");

	button c_time;
	c_time.width = 100;
	c_time.height = 60;
	c_time.colour = CYAN;
	c_time.has_text = 1;
	c_time.valid = 1;
	strcpy(c_time.message, "Time");

	ClearScreen(BLACK);
	drawButton_two_lines(map);
	drawButton_two_lines(load);
	DrawMap();

	keyboard k = initializeKeyboard();
	//textbox t = initializeTextbox();
	building to_work;

	int numbers = 0;
	int in_info_page = 0;
	int update_index;
	int new_building = 0;
	int building_pressed;
	int button_choice;
	// Toggle from one menu to another by pressing the new_location button.
	while (1) {
		p1 = GetRelease();

		if(!in_info_page){
			if(isButtonPressed(p1,load)){
				BUILDING_EXIST = 1;
				numbers = init_sdcard(0,1);
				new_building = 1;
			}

			else if (isButtonPressed(p1,map)){
				char* nmea_string = (char*)malloc(sizeof(char));
				read_one_data_entry (nmea_string);
				printf("%s\n", nmea_string);
				while(test_GPGGA(nmea_string) == FALSE){
					free(nmea_string);
					char* nmea_string = (char*)malloc(sizeof(char));
					read_one_data_entry (nmea_string);
					printf("%s\n", nmea_string);
				}
				GPS_info info = parse_GPS_packet (nmea_string);

				touchscreen_coor onscreen = building_coord(info,B_HEIGHT,B_WIDTH);
				BUILDING_EXIST=1;
				building build = create_building(onscreen);
				initialize_building_info(build.msg);
				ubc_buildings[numbers]=build;
				//numbers = addBuildingSD(ubc_buildings[numbers]);
				new_building = 1;
				numbers++;
			}

			if(new_building){
				if(BUILDING_EXIST){
					int count;
					for(count = 0; count <= numbers; count++){
						drawButton(ubc_buildings[count].box);
					}
				}
			}

			for(update_index = 0; update_index < numbers; update_index++){
				if(isButtonPressed(p1,ubc_buildings[update_index].box) && !k.isKeyboardOn){
					to_work = ubc_buildings[update_index];
					FramedRectangle(ubc_buildings[update_index].box.x_start, ubc_buildings[update_index].box.y_start, B_WIDTH, B_HEIGHT, B_SELECTED_COLOUR, B_SELECTED_COLOUR);
					if( ubc_buildings[update_index].box.x_start < 400 ){
						FramedRectangle(400, 0, 400,480,WHITE,WHITE);
						name.x_start = 420;
						name.y_start = 30;
						usage.x_start = 420;
						usage.y_start = 120;
						c_code.x_start= 420;
						c_code.y_start = 200;
						c_date.x_start = 420;
						c_date.y_start = 280;
						c_time.x_start = 420;
						c_time.y_start = 360;
						printString(ubc_buildings[update_index].msg.name, 530, 80, BLACK, CYAN);
						printString(ubc_buildings[update_index].msg.building_usage, 530, 160 ,BLACK, CYAN);
						printString(ubc_buildings[update_index].msg.course, 530, 240 ,BLACK, CYAN);
						printString(ubc_buildings[update_index].msg.course_date, 530, 320,BLACK, CYAN);
						printString(ubc_buildings[update_index].msg.course_time, 530, 400,BLACK, CYAN);
						back.y_start = 420;
						back.x_start = 700;
					}
					else
					{
						FramedRectangle(0, 0, 400, 480, WHITE, WHITE);
						name.x_start = 20;
						name.y_start = 30;
						usage.x_start = 20;
						usage.y_start = 120;
						c_code.x_start = 20;
						c_code.y_start = 200;
						c_date.x_start = 20;
						c_date.y_start = 280;
						c_time.x_start = 20;
						c_time.y_start = 360;
						printString(ubc_buildings[update_index].msg.name, 130,
								80, BLACK, CYAN);
						printString(
								ubc_buildings[update_index].msg.building_usage,
								130, 160, BLACK, CYAN);
						printString(ubc_buildings[update_index].msg.course, 130,
								240, BLACK, CYAN);
						printString(ubc_buildings[update_index].msg.course_date,
								130, 320, BLACK, CYAN);
						printString(ubc_buildings[update_index].msg.course_time,
								130, 400, BLACK, CYAN);
						back.y_start = 420;
						back.x_start = 300;
					}
					drawButton(name);
					drawButton(usage);
					drawButton(c_code);
					drawButton(c_time);
					drawButton(c_date);
					drawButton(back);
					in_info_page = 1;
					building_pressed = update_index;
					break;
				}


			}
		}

		else {
			if(isButtonPressed(p1,back)){
				in_info_page = 0;
				if(BUILDING_EXIST){
					int count;
					for(count = 0; count <= numbers; count++){
						drawButton(ubc_buildings[count].box);
					}
				}
				ClearScreen(BLACK);
				drawButton_two_lines(map);
				drawButton_two_lines(load);
				DrawMap();
				if (BUILDING_EXIST) {
					int count;
					for (count = 0; count <= numbers; count++) {
						drawButton(ubc_buildings[count].box);
					}
				}
			}
			else if(isButtonPressed(p1,name)){
				button_choice = 1;
				to_work.info = initializeTextbox();
				if (k.isKeyboardOn == 1) {
					k.isKeyboardOn = 0;
					to_work.info.isTextboxOn = 0;
				}
				else {
					k.isKeyboardOn = 1;
					to_work.info.isTextboxOn = 1;
				}

			}

			else if (isButtonPressed(p1, usage)) {
				button_choice = 2;
				to_work.info = initializeTextbox();
				if (k.isKeyboardOn == 1) {
					k.isKeyboardOn = 0;
					to_work.info.isTextboxOn = 0;
				} else {
					k.isKeyboardOn = 1;
					to_work.info.isTextboxOn = 1;
				}

			}

			else if (isButtonPressed(p1, c_code)) {
				button_choice = 3;
				to_work.info = initializeTextbox();
				if (k.isKeyboardOn == 1) {
					k.isKeyboardOn = 0;
					to_work.info.isTextboxOn = 0;
				} else {
					k.isKeyboardOn = 1;
					to_work.info.isTextboxOn = 1;
				}

			}

			else if (isButtonPressed(p1, c_date)) {
				button_choice = 4;
				to_work.info = initializeTextbox();
				if (k.isKeyboardOn == 1) {
					k.isKeyboardOn = 0;
					to_work.info.isTextboxOn = 0;
				} else {
					k.isKeyboardOn = 1;
					to_work.info.isTextboxOn = 1;
				}

			}

			else if (isButtonPressed(p1, c_time)) {
				button_choice = 5;
				to_work.info = initializeTextbox();
				if (k.isKeyboardOn == 1) {
					k.isKeyboardOn = 0;
					to_work.info.isTextboxOn = 0;
				} else {
					k.isKeyboardOn = 1;
					to_work.info.isTextboxOn = 1;
				}

			}

			if (k.isKeyboardOn) {
				ClearScreen(BLACK);
				drawTextbox(to_work.info);
				drawKeyboard(&k);

				int isThereSomethingToRead = 0;
				// checks if the touch was on a keyboard key. If it was, it preforms the
				// action of that key.
				isThereSomethingToRead = keyboardListener(p1, &k, &to_work.info);

				if (isThereSomethingToRead) {
					printf("t.last_string contains string: %s\n", to_work.info.last_saved_string);
					if(button_choice == 1)
						strcpy(ubc_buildings[building_pressed].msg.name, to_work.info.last_saved_string);
					if(button_choice == 2)
						strcpy(ubc_buildings[building_pressed].msg.building_usage, to_work.info.last_saved_string);
					if(button_choice == 3)
						strcpy(ubc_buildings[building_pressed].msg.course, to_work.info.last_saved_string);
					if(button_choice == 4)
						strcpy(ubc_buildings[building_pressed].msg.course_date, to_work.info.last_saved_string);
					if(button_choice == 5)
						strcpy(ubc_buildings[building_pressed].msg.course_time, to_work.info.last_saved_string);

					k.isKeyboardOn = 0;
					to_work.info.isTextboxOn = 0;
					in_info_page = 0;
					ClearScreen(BLACK);
					drawButton_two_lines(map);
					drawButton_two_lines(load);
					DrawMap();
					if (BUILDING_EXIST) {
						int count;
						for (count = 0; count <= numbers; count++) {
							drawButton(ubc_buildings[count].box);
						}
					}
				}
				else {
					ClearScreen(BLACK);
									drawTextbox(to_work.info);
									drawKeyboard(&k);
				}
			}

		}



		wait_xk_loops(200);
		int jk;
		for(jk = 0; jk < number_of_buildings; jk++)
			storeBuildingSD(ubc_buildings[jk], jk);
	}

	return 0;

}

