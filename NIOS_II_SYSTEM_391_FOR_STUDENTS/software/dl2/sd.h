/*
 * sd.h
 *
 *  Created on: 2016-02-25
 *      Author: Tanner
 */

#ifndef SD_H_
#define SD_H_

#include <altera_up_sd_card_avalon_interface.h>
#include <stdio.h>

int number_of_buildings = 0;

typedef struct {
	short int xcoordh;
	short int xcoordl;
	short int ycoordh;
	short int ycoordl;
	char name[MAX];
	char course[MAX];
	char course_date[MAX];
	char course_time[MAX];
	char building_usage[MAX];
} savedbuilding;

char *filearray[] = { "b01.txt", "b02.txt", "b03.txt", "b04.txt", "b05.txt",
		"b06.txt", "b07.txt", "b08.txt", "b09.txt", "b00.txt" };

int SDconnect(int debug) {
	alt_up_sd_card_dev *device_reference = 0;
	int connected = 0;
	if(debug) printf("Opening SDCard\n");
	if ((device_reference = alt_up_sd_card_open_dev(
			"/dev/Altera_UP_SD_Card_Avalon_Interface_0")) == 0) {
		if(debug) printf("SDCard Open FAILED\n");
		return 0;
	} else
		if(debug) printf("SDCard Open PASSED\n");
	if (device_reference != 0) {
		while (1) {
			if ((connected == 0) && (alt_up_sd_card_is_Present())) {
				if(debug) printf("Card connected.\n");
				if (alt_up_sd_card_is_FAT16()) {
					if(debug) printf("FAT16 file system detected.\n");
					goto done;
				} else {
					if(debug) printf("Unknown file system.\n");
					return -1;
				}
				connected = 1;
			} else if ((connected == 1) && (alt_up_sd_card_is_Present() == 0)) {
				if(debug) printf("Card disconnected.\n");
				connected = 0;
				return -2;
			}
		}
	} else
		if(debug) printf("Can't open device\n");
	return -3;
	done: return 1;
}

short int getBuildingCount(void) {
	short int fd;
	short int numBuildings;
	short int error;
	if (alt_up_sd_card_is_Present() && alt_up_sd_card_is_FAT16()) {
		if ((fd = alt_up_sd_card_fopen("build.txt", 0)) != -1) {
			numBuildings = alt_up_sd_card_read(fd);
		} else {
			error = alt_up_sd_card_fclose(fd);
			return -5;
		}
	}
	error = alt_up_sd_card_fclose(fd);
	if (error < 0)
		return error;
	return numBuildings;
}
int setBuildingCount(short int numBuildings) {
	short int fd;
	int error;
	if (alt_up_sd_card_is_Present() && alt_up_sd_card_is_FAT16()) {
		if ((fd = alt_up_sd_card_fopen("build.txt", 0)) != -1) {
			error = alt_up_sd_card_write(fd, numBuildings);
		} else {
			error = alt_up_sd_card_fclose(fd);
			return -6;
		}
	}
	error = alt_up_sd_card_fclose(fd);
	if (error < 0)
		return error;
	return numBuildings;
}

int saveBuilding(int index, savedbuilding sbuilding) {
	short int fd;
	int i = 0;
	int error;
	if (alt_up_sd_card_is_Present() && alt_up_sd_card_is_FAT16()) {
		if ((fd = alt_up_sd_card_fopen(filearray[index], 0)) != -1) {
			error = alt_up_sd_card_write(fd, sbuilding.xcoordl);
			error = alt_up_sd_card_write(fd, sbuilding.xcoordh);
			error = alt_up_sd_card_write(fd, sbuilding.ycoordl);
			error = alt_up_sd_card_write(fd, sbuilding.ycoordh);
			while (i < MAX) {
				error = alt_up_sd_card_write(fd, sbuilding.name[i]);
				i++;
			}
			i = 0;
			while (i < MAX) {
				error = alt_up_sd_card_write(fd, sbuilding.course_date[i]);
				i++;
			}
			i = 0;
			while (i < MAX) {
				error = alt_up_sd_card_write(fd, sbuilding.course[i]);
				i++;
			}
			i = 0;
			while (i < MAX) {
				error = alt_up_sd_card_write(fd, sbuilding.course_time[i]);
				i++;
			}
			i = 0;
			while (i < MAX) {
				error = alt_up_sd_card_write(fd, sbuilding.building_usage[i]);
				i++;
			}
		} else {
			error = alt_up_sd_card_fclose(fd);
			return -6;
		}
	}
	error = alt_up_sd_card_fclose(fd);
	if (error < 0)
		return error;
	return index;
}

savedbuilding loadBuilding(int index) {
	short int fd;
	short int error;
	char buf;
	int i = 0;
	savedbuilding sb;
	if (alt_up_sd_card_is_Present() && alt_up_sd_card_is_FAT16()) {
		if ((fd = alt_up_sd_card_fopen(filearray[index], 0)) != -1) {
			sb.xcoordl = alt_up_sd_card_read(fd);
			sb.xcoordh = alt_up_sd_card_read(fd);
			sb.ycoordl = alt_up_sd_card_read(fd);
			sb.ycoordh = alt_up_sd_card_read(fd);
			buf = alt_up_sd_card_read(fd);
			while (i < MAX) {
				sb.name[i] = buf;
				buf = alt_up_sd_card_read(fd);
				i++;
			}
			i = 0;
			while (i < MAX) {
				sb.course_date[i] = buf;
				buf = alt_up_sd_card_read(fd);
				i++;
			}
			i = 0;
			while (i < MAX) {
				sb.course[i] = buf;
				buf = alt_up_sd_card_read(fd);
				i++;
			}
			i = 0;
			while (i < MAX) {
				sb.course_time[i] = buf;
				buf = alt_up_sd_card_read(fd);
				i++;
			}
			i = 0;
			while (i < MAX) {
				sb.building_usage[i] = buf;
				buf = alt_up_sd_card_read(fd);
				i++;
			}

		} else {
			error = alt_up_sd_card_fclose(fd);
		}
	}

	error = alt_up_sd_card_fclose(fd);

	alt_up_sd_card_fclose(fd);
	return sb;
}

int initBuildingArray() {
	number_of_buildings = getBuildingCount();
	int i;
	savedbuilding sb;
	for (i = 0; i < number_of_buildings; i++) {
		sb = loadBuilding(i);
		touchscreen_coor cords;
		cords.touchscreen_x = (sb.xcoordh << 8) | sb.xcoordl;
		cords.touchscreen_y = (sb.ycoordh << 8) | sb.ycoordl;
		ubc_buildings[i] = create_building(cords);
		strcpy(ubc_buildings[i].msg.course_time, sb.course_time);
		strcpy(ubc_buildings[i].msg.course_date, sb.course_date);
		strcpy(ubc_buildings[i].msg.name, sb.name);
		strcpy(ubc_buildings[i].msg.course, sb.course);
		strcpy(ubc_buildings[i].msg.building_usage, sb.building_usage);
	}
	return number_of_buildings;
}
int storeBuildingSD(building bld, int index) {
	savedbuilding sb;
	strcpy(sb.course_time, bld.msg.course_time);
	strcpy(sb.course_date, bld.msg.course_date);
	strcpy(sb.course, bld.msg.course);
	strcpy(sb.name, bld.msg.name);
	strcpy(sb.building_usage, bld.msg.building_usage);
	sb.xcoordl = (bld.box.x_start) & 0x00FF;
	sb.xcoordh = ((bld.box.x_start) & 0xFF00) >> 8;
	sb.ycoordl = (bld.box.y_start) & 0x00FF;
	sb.ycoordh = ((bld.box.y_start) & 0xFF00) >> 8;

	return saveBuilding(index, sb);
}
int addBuildingSD(building bld) {
	number_of_buildings++;
	setBuildingCount (number_of_buildings);
	return storeBuildingSD(bld, number_of_buildings - 1);
}
// preload : load files onto sd card
// debug : show output
int init_sdcard(int preload, int debug) {
	SDconnect(debug);
	int i = 0;
	if (preload ) {
		building b;
		touchscreen_coor cords;
		cords.touchscreen_x = 190;
		cords.touchscreen_y = 112;

		b = create_building(cords);
		strcpy(b.msg.course_date, "Monday");
		strcpy(b.msg.course, "COMM 411");
		strcpy(b.msg.name, "Henry Angus");
		strcpy(b.msg.building_usage, "Business");
		strcpy(b.msg.course_time, "8:10");
		addBuildingSD(b);

		cords.touchscreen_x = 144;
		cords.touchscreen_y = 76;
		b = create_building(cords);
		strcpy(b.msg.course_date, "Tuesday");
		strcpy(b.msg.course, "GEOG 234");
		strcpy(b.msg.name, "math");
		strcpy(b.msg.building_usage, "Lectures");
		strcpy(b.msg.course_time, "3:30");
		addBuildingSD(b);

		cords.touchscreen_x = 370;
		cords.touchscreen_y = 209;
		b = create_building(cords);
		strcpy(b.msg.course_date, "Wednesday");
		strcpy(b.msg.course, "CPSC 340");
		strcpy(b.msg.name, "CHBE ");
		strcpy(b.msg.building_usage, "lectures");
		strcpy(b.msg.course_time, "9:30");
		addBuildingSD(b);

		cords.touchscreen_x = 179;
		cords.touchscreen_y = 309;
		b = create_building(cords);
		strcpy(b.msg.course_date, "Thursday");
		strcpy(b.msg.course, "ARTS 102");
		strcpy(b.msg.name, "Totem");
		strcpy(b.msg.building_usage, "Library");
		strcpy(b.msg.course_time, "5:20");
		addBuildingSD(b);

		cords.touchscreen_x = 407;
		cords.touchscreen_y = 151;
		b = create_building(cords);
		strcpy(b.msg.course_date, "Friday");
		strcpy(b.msg.course, "cpen 391");
		strcpy(b.msg.name, "Hospital");
		strcpy(b.msg.building_usage, "EECE");
		strcpy(b.msg.course_time, "12:10");
		addBuildingSD(b);
	}
	int numbers = initBuildingArray();
	if( debug ){
		for (i = 0; i < number_of_buildings; i++) {
			printf("\nCourse Date: ");
			printf(ubc_buildings[i].msg.course_date);
			printf(" || Building Name: ");
			printf(ubc_buildings[i].msg.name);
			printf(" || Course:  ");
			printf(ubc_buildings[i].msg.course);
			printf(" || Course time:  ");
			printf(ubc_buildings[i].msg.course_time);
			printf(" || Building Use:  ");
			printf(ubc_buildings[i].msg.building_usage);
			printf(" || x cord:  ");
			printf("%d", ubc_buildings[i].box.x_start);
			printf(" || y cord:  ");
			printf("%d", ubc_buildings[i].box.y_start);
		}
	}

	return numbers;
}


#endif /* SD_H_ */
