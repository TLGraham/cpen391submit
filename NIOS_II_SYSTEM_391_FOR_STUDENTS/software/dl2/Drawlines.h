/*
 * Drawlines.h
 *
 *  Created on: 2016-02-24
 *      Author: k8w9a
 */

#ifndef DRAWLINES_H_
#define DRAWLINES_H_

#include "GPS.h"

// graphics registers all address begin with '8' so as to by pass data cache on NIOS

#define  GraphicsCommandReg           (*(volatile unsigned short int *)(0x84000000))
#define  GraphicsStatusReg            (*(volatile unsigned short int *)(0x84000000))
#define  GraphicsX1Reg                (*(volatile unsigned short int *)(0x84000002))
#define  GraphicsY1Reg                (*(volatile unsigned short int *)(0x84000004))
#define  GraphicsX2Reg                (*(volatile unsigned short int *)(0x84000006))
#define  GraphicsY2Reg                (*(volatile unsigned short int *)(0x84000008))
#define  GraphicsColourReg            (*(volatile unsigned short int *)(0x8400000E))
#define  GraphicsBackGroundColourReg  (*(volatile unsigned short int *)(0x84000010))

/************************************************************************************************
 ** This macro pauses until the graphics chip status register indicates that it is idle
 ***********************************************************************************************/

#define WAIT_FOR_GRAPHICS    while ((GraphicsStatusReg & 0x0001) != 0x0001);

//Here are some software routines to drive some simple graphics functions

// #defined constants representing values we write to the graphics 'command' register to get
// it to draw something. You will add more values as you add hardware to the graphics chip
// Note DrawHLine, DrawVLine and DrawLine at the moment do nothing - you will modify these

#define DrawHLine             1
#define DrawVLine             2
#define DrawLine              3
#define PutAPixel             0xA
#define GetAPixel             0xB
#define ProgramPaletteColour  0x10


// defined constants representing colours pre-programmed into colour palette
// there are 256 colours but only 8 are shown below, we write these to the colour registers
//
// the header files "Colours.h" contains constants for all 256 colours
// while the course file ColourPaletteData.c contains the 24 bit RGB data
// that is pre-programmed into the palette

#define BLACK    0
#define WHITE    1
#define RED      2
#define LIME     3
#define BLUE     4
#define YELLOW   5
#define CYAN     6
#define MAGENTA  7

/*******************************************************************************************
 * This function writes a single pixel to the x,y coords specified using the specified colour
 * Note colour is a byte and represents a palette number (0-255) not a 24 bit RGB value
 ********************************************************************************************/
void WriteAPixel(int x, int y, int Colour) {
    WAIT_FOR_GRAPHICS;              // is graphics ready for new command

    GraphicsX1Reg = x;              // write coords to x1, y1
    GraphicsY1Reg = y;

    GraphicsColourReg = Colour;     // set pixel colour
    GraphicsCommandReg = PutAPixel; // give graphics "write pixel" command
}

/*********************************************************************************************
 * This function read a single pixel from the x,y coords specified and returns its colour
 * Note returned colour is a byte and represents a palette number (0-255) not a 24 bit RGB value
 *********************************************************************************************/

int ReadAPixel(int x, int y) {
    WAIT_FOR_GRAPHICS;                // is graphics ready for new command

    GraphicsX1Reg = x;                // write coords to x1, y1
    GraphicsY1Reg = y;

    GraphicsCommandReg = GetAPixel;   // give graphics a "get pixel" command

    WAIT_FOR_GRAPHICS;                // is graphics done reading pixel
    return (int)(GraphicsColourReg);  // return the palette number (colour)
}


/**********************************************************************************
 ** subroutine to program a hardware (graphics chip) palette number with an RGB value
 ** e.g. ProgramPalette(RED, 0x00FF0000) ;
 **
 ************************************************************************************/

void ProgramPalette(int PaletteNumber, int RGB) {
    WAIT_FOR_GRAPHICS;
    GraphicsColourReg  = PaletteNumber;
    GraphicsX1Reg      = RGB >> 16;              // program red value in ls.8 bit of X1 reg
    GraphicsY1Reg      = RGB;                    // program green and blue into ls 16 bit of Y1 reg
    GraphicsCommandReg = ProgramPaletteColour;   // issue command
}

/*********************************************************************************************
 This function draw a horizontal line, 1 pixel at a time starting at the x,y coords specified
 *********************************************************************************************/

void HLine(int x1, int y1, int length, int Colour) {
    WAIT_FOR_GRAPHICS;
    GraphicsX1Reg = x1;
    GraphicsY1Reg = y1;
    GraphicsX2Reg = x1 + length;
    GraphicsColourReg = Colour;
    GraphicsCommandReg = DrawHLine;
}

/*********************************************************************************************
 This function draw a vertical line, 1 pixel at a time starting at the x,y coords specified
 *********************************************************************************************/

void VLine(int x1, int y1, int length, int Colour) {
    WAIT_FOR_GRAPHICS;
    GraphicsX1Reg = x1;
    GraphicsY1Reg = y1;
    GraphicsY2Reg = y1 + length;
    GraphicsColourReg = Colour;
    GraphicsCommandReg = DrawVLine;
}

/*******************************************************************************
 ** Implementation of Bresenhams line drawing algorithm
 *******************************************************************************/

void FramedRectangle(int x1, int y1, int width, int height, int framecolour, int fillcolour ) {

    HLine(x1, y1, width, framecolour);
    int tempy1;

    for (tempy1 = y1+1; tempy1 < y1+height; tempy1++) {
        HLine(x1+1, tempy1, width-1,fillcolour);
    }

    HLine(x1, y1+height, width, framecolour);
    VLine(x1, y1, height-1, framecolour);
    VLine(x1 + width, y1, height-1, framecolour);

}


int abs(int a)
{
    if(a < 0)
        return -a ;
    else
        return a ;
}

int sign(int a)
{
    if(a < 0)
        return -1 ;
    else if (a == 0)
        return 0 ;
    else
        return 1 ;
}


void Line(int x1, int y1, int x2, int y2, int Colour)
{
    int x = x1;
    int y = y1;
    int dx = abs(x2 - x1);
    int dy = abs(y2 - y1);

    int s1 = sign(x2 - x1);
    int s2 = sign(y2 - y1);
    int i, temp, interchange = 0, error ;

    // if x1=x2 and y1=y2 then it is a line of zero length

    if(dx == 0 && dy == 0)
        return ;

    // must be a complex line so use bresenhams algorithm
    else    {

        // swap delta x and delta y depending upon slop of line

        if(dy > dx) {
            temp = dx ;
            dx = dy ;
            dy = temp ;
            interchange = 1 ;
        }

        // initialise the error term to compensate for non-zero intercept

        error = (dy << 1) - dx ;    // (2 * dy) - dx

        // main loop
        for(i = 1; i <= dx; i++)    {
            WriteAPixel(x, y, Colour);

            while(error >= 0)   {
                if(interchange == 1)
                    x += s1 ;
                else
                    y += s2 ;

                error -= (dx << 1) ;    // times 2
            }

            if(interchange == 1)
                y += s2 ;
            else
                x += s1 ;

            error += (dy << 1) ;    // times 2
        }
    }
}


void ClearScreen(int colour) {
    FramedRectangle(0, 0, 800, 480, colour, colour);
}

void DrawMap(void){
    degree_coor em_point1;
    degree_coor em_point2;
    touchscreen_coor ts_em_point1;
    touchscreen_coor ts_em_point2;

    degree_coor mm_point1;
    degree_coor mm_point2;
    touchscreen_coor ts_mm_point1;
    touchscreen_coor ts_mm_point2;

    degree_coor wm_point1;
    degree_coor wm_point2;
    touchscreen_coor ts_wm_point1;
    touchscreen_coor ts_wm_point2;

    degree_coor lm_point1;
    degree_coor lm_point2;
    touchscreen_coor ts_lm_point1;
    touchscreen_coor ts_lm_point2;

    degree_coor ag_point1;
    degree_coor ag_point2;
    touchscreen_coor ts_ag_point1;
    touchscreen_coor ts_ag_point2;

    degree_coor th_point1;
    degree_coor th_point2;
    touchscreen_coor ts_th_point1;
    touchscreen_coor ts_th_point2;

    em_point1.latitude = 49.269032;
    em_point1.longitude = -123.253191;
    em_point2.latitude = 49.255662;
    em_point2.longitude = -123.242167;

    mm_point1.latitude = 49.269112;
    mm_point1.longitude = -123.256179;
    mm_point2.latitude = 49.255828;
    mm_point2.longitude = -123.245292;

    wm_point1.latitude = 49.268550;
    wm_point1.longitude = -123.258794;
    wm_point2.latitude = 49.255788;
    wm_point2.longitude = -123.248344;

    lm_point1.latitude = 49.266483;
    lm_point1.longitude = -123.259074;
    lm_point2.latitude = 49.259320;
    lm_point2.longitude = -123.253051;

    ag_point1.latitude = 49.263983;
    ag_point1.longitude = -123.239487;
    ag_point2.latitude = 49.259827;
    ag_point2.longitude = -123.251417;

    th_point1.latitude = 49.261637;
    th_point1.longitude = -123.241718;
    th_point2.latitude = 49.258378;
    th_point2.longitude = -123.251245;


    ts_em_point1.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+em_point1.longitude)/LONG_RANGE+0.4);
    ts_em_point1.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-em_point1.latitude)/LAT_RANGE+0.4);
    ts_em_point2.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+em_point2.longitude)/LONG_RANGE+0.4);
    ts_em_point2.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-em_point2.latitude)/LAT_RANGE+0.4);


    ts_mm_point1.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+mm_point1.longitude)/LONG_RANGE+0.4);
    ts_mm_point1.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-mm_point1.latitude)/LAT_RANGE+0.4);
    ts_mm_point2.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+mm_point2.longitude)/LONG_RANGE+0.4);
    ts_mm_point2.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-mm_point2.latitude)/LAT_RANGE+0.4);


    ts_wm_point1.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+wm_point1.longitude)/LONG_RANGE+0.4);
    ts_wm_point1.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-wm_point1.latitude)/LAT_RANGE+0.4);
    ts_wm_point2.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+wm_point2.longitude)/LONG_RANGE+0.4);
    ts_wm_point2.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-wm_point2.latitude)/LAT_RANGE+0.4);

    ts_lm_point1.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+lm_point1.longitude)/LONG_RANGE+0.4);
    ts_lm_point1.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-lm_point1.latitude)/LAT_RANGE+0.4);
    ts_lm_point2.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+lm_point2.longitude)/LONG_RANGE+0.4);
    ts_lm_point2.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-lm_point2.latitude)/LAT_RANGE+0.4);

    ts_ag_point1.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+ag_point1.longitude)/LONG_RANGE+0.4);
    ts_ag_point1.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-ag_point1.latitude)/LAT_RANGE+0.4);
    ts_ag_point2.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+ag_point2.longitude)/LONG_RANGE+0.4);
    ts_ag_point2.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-ag_point2.latitude)/LAT_RANGE+0.4);

    ts_th_point1.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+th_point1.longitude)/LONG_RANGE+0.4);
    ts_th_point1.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-th_point1.latitude)/LAT_RANGE+0.4);
    ts_th_point2.touchscreen_x = (int)(WIDTH*(TOP_LEFT_CORNER_LON+th_point2.longitude)/LONG_RANGE+0.4);
    ts_th_point2.touchscreen_y = (int)(HEIGHT*(TOP_LEFT_CORNER_LAT-th_point2.latitude)/LAT_RANGE+0.4);

    Line(ts_em_point1.touchscreen_x, ts_em_point1.touchscreen_y, ts_em_point2.touchscreen_x, ts_em_point2.touchscreen_y, WHITE);
    Line(ts_mm_point1.touchscreen_x, ts_mm_point1.touchscreen_y, ts_mm_point2.touchscreen_x, ts_mm_point2.touchscreen_y, WHITE);
    Line(ts_wm_point1.touchscreen_x, ts_wm_point1.touchscreen_y, ts_wm_point2.touchscreen_x, ts_wm_point2.touchscreen_y, WHITE);
    Line(ts_lm_point1.touchscreen_x, ts_lm_point1.touchscreen_y, ts_lm_point2.touchscreen_x, ts_lm_point2.touchscreen_y, WHITE);
    Line(ts_ag_point1.touchscreen_x, ts_ag_point1.touchscreen_y, ts_ag_point2.touchscreen_x, ts_ag_point2.touchscreen_y, WHITE);
    Line(ts_th_point1.touchscreen_x, ts_th_point1.touchscreen_y, ts_th_point2.touchscreen_x, ts_th_point2.touchscreen_y, WHITE);

}


#endif /* DRAWLINES_H_ */
