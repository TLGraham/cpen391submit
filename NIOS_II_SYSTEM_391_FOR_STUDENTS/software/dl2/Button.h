/*
 * Button.h
 *
 *  Created on: 2016-02-25
 *      Author: k8w9a
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include "TouchScreen.h"
#include "Fonts.h"
#include "Drawlines.h"

#define MAX_STRING 64
#define CHAR_WIDTH 12
#define CHAR_HEIGHT 14
#define XRES 800
#define YRES 480

typedef struct {
  int x_start;
  int y_start;
  int width;
  int height;
  int colour;
  int valid;
  int has_text;
  char message[MAX_STRING];   // Button text on first line
  char message2[MAX_STRING];  // Button text on second line (used only for
                              // drawing a two line button)
} button;

typedef struct {
  button box;
  int cursor_pos;                       // Used for backspacing
  int isTextboxOn;                     // Turns on and off the textbox
  char last_saved_string[MAX_STRING];   // holds the string that was saved with
                                        // an enter key
} textbox;

int isButtonPressed(Point p, button b) {
  if (p.x >= b.x_start && p.x <= b.x_start+b.width &&
      p.y >= b.y_start && p.y <= b.y_start+b.height) {

#ifdef DEBUG
    printf("button id #%d Pressed\n", b.id);
#endif // DEBUG
    return 1;
  }
  return 0;

}

void OutGraphicsCharFont1(int x, int y, int fontcolour, int backgroundcolour,
                          int c, int erase) {

  register int row, column, theX = x, theY = y;
  register int pixels;
  register char theColour = fontcolour;
  register int BitMask, theC = c;

  // if x, y coord are off screen, dont bother
  if (((short)(x) > (short)(XRES-1)) || ((short)(y) > (short)(YRES-1))) {
    return;
  }

  // if printable character subtract hex 20
  if (((short)(theC) >= (short)(' ')) && ((short)(theC) <= (short)('~'))) {
    theC = theC - 0x20;

    for (row = 0; (char)(row) < (char)(14); row++) {
      pixels = Font10x14[theC][row];
      BitMask = 0x200;

      for (column = 0; (char)(column) < (char)(10); column++) {
        if ((pixels & BitMask)) {
          WriteAPixel(theX+column, theY+row, theColour);
        }
        else {
          if (erase == 1) {
            WriteAPixel(theX+column, theY+row, backgroundcolour);
          }
        }
        BitMask = BitMask >> 1;
      }
    }
  }
}

void printString(char* string, int x_start, int y_start,
                 int char_colour, int background_colour) {
  int i;
  int x = x_start;
  int y = y_start;
  const int char_width = 10;

  for (i = 0; i < strlen(string); i++) {
#ifdef DEBUG
    printf("string[%d] = %c\n", i, string[i]);
#endif // DEBUG
    OutGraphicsCharFont1(x, y, char_colour, background_colour, string[i], 0);
    x = x + char_width + 2;
  }
}

// Draws a button with the text centered on one line
void drawButton(button b) {
  FramedRectangle(b.x_start, b.y_start, b.width, b.height, BLACK, b.colour);

  if(b.has_text){
	  int x_string = b.x_start + ((b.width - CHAR_WIDTH*strlen(b.message))/2);
	  int y_string = b.y_start + ((b.height - CHAR_HEIGHT)/2);
	  printString(b.message, x_string, y_string, BLACK, b.colour);
  }
}

// Draws the textbox with text.
void drawTextbox(textbox t) {
  if (t.isTextboxOn) {
    drawButton(t.box);
  }
}

// Draws a button with the button text centered on two lines
void drawButton_two_lines(button b) {
  FramedRectangle(b.x_start, b.y_start, b.width, b.height, BLACK, b.colour);

  int x_string = b.x_start + ((b.width - CHAR_WIDTH*strlen(b.message))/2);
  int y_string = b.y_start + ((b.height - CHAR_HEIGHT*2+2)/2);
  printString(b.message, x_string, y_string, BLACK, b.colour);

  x_string = b.x_start + ((b.width - CHAR_WIDTH*strlen(b.message2))/2);
  y_string += CHAR_HEIGHT+2;
  printString(b.message2, x_string, y_string, BLACK, b.colour);
}

textbox initializeTextbox() {
  textbox t;

  t.cursor_pos = 0;
  t.isTextboxOn = 0;

  t.box.x_start = 150;
  t.box.y_start = 135;
  t.box.width = 500;
  t.box.height = 30;
  t.box.colour = WHITE;
  t.box.valid = 1;
  strcpy(t.box.message, "");

  return t;
}

#endif /* BUTTON_H_ */
