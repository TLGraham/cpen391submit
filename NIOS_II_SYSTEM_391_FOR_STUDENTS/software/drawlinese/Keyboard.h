/*
 * Keyboard.h
 *
 *  Created on: 2016-02-24
 *      Author: k8w9a
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include "Button.h"
//Keyboard constants
#define NUMLETTERS                 26
#define BUTTON_H                   60
#define BUTTON_W                   60
#define FIRST_LETTER_ROW_CUTOFF    9
#define SECOND_LETTER_ROW_CUTOFF   18

#define NUMPUNCTNUM 26
#define FIRST_PUNCTNUM_ROW_CUTOFF  9
#define SECOND_PUNCTNUM_ROW_CUTOFF 18

#define KEY_COLOUR WHITE
#define KEYBOARD_BACKGROUND_COLOUR BLACK

char* upperletters_str[NUMLETTERS] = {"Q","W","E","R","T","Y","U","I","O","P",
                                        "A","S","D","F","G","H","J","K","L",
                                           "Z","X","C","V","B","N","M"};

char* lowerletters_str[NUMLETTERS] = {"q","w","e","r","t","y","u","i","o","p",
                                        "a","s","d","f","g","h","j","k","l",
                                           "z","x","c","v","b","n","m"};

char* num_punc_str[NUMPUNCTNUM] = {"1","2","3","4","5","6","7","8","9","0",
                                     "-","/",":",";","(",")","$","&","@",
                                        "\"",".",",","?","!","\'",""};

// we can use this to check when a button is pressed...
// ie if (isButtonPressed(pressPoint, k.letters[A]))
typedef enum { Q,W,E,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M } alpha;



typedef struct {
  button letters[NUMLETTERS];             // Letter buttons
  button numbers_and_punct[NUMPUNCTNUM];  // number and punctuation buttons
  button enter;
  button space;
  button shift;
  button backspace;
  button number_letter_toggle;
  short int isCapsOn;                          // When true, the capital letter keys
                                          // are drawn when drawKeyboard is called

  short int isNumbersOn;                       // When true, the number and
                                          // punctuation keys are drawn when
                                          // drawKeyboard is called

  short int isKeyboardOn;                      // When true, the keyboard is drawn
                                          // when drawKeyboard is called
} keyboard;

void drawKeyboard(keyboard *k) {
  if (k->isKeyboardOn) {
    int i;
    if (k->isNumbersOn) {
      // Draw numbers
      for (i = 0; i < NUMPUNCTNUM; i++) {
        drawButton(k->numbers_and_punct[i]);
      }
      strcpy(k->number_letter_toggle.message, "abc");
      drawButton(k->number_letter_toggle);
    }
    else if (k->isCapsOn) {
      // draw Capital Letters
      for (i = 0; i < NUMLETTERS; i++) {
        strcpy(k->letters[i].message, upperletters_str[i]);
        drawButton(k->letters[i]);
      }
      strcpy(k->number_letter_toggle.message, "123");
      drawButton(k->number_letter_toggle);
    }
    else {
      // draw lowercase letters;
      for (i = 0; i < NUMLETTERS; i++) {
        strcpy(k->letters[i].message, lowerletters_str[i]);
        drawButton(k->letters[i]);
      }
      strcpy(k->number_letter_toggle.message, "123");
      drawButton(k->number_letter_toggle);
    }

    drawButton(k->space);
    drawButton(k->enter);
    drawButton(k->shift);
    drawButton(k->backspace);
  }
}

keyboard initializeKeyboard() {
  keyboard k;
  k.isCapsOn = 0;
  k.isNumbersOn = 0;
  k.isKeyboardOn = 0;

  int i;
  //int rownum = 1;
  int button_x = 55;
  int button_y = 175;

  // The buttons for the letters. Note that the same buttons are used for
  // capital and lowercase letters... What they buttons print and display is up
  // to the state that the isCapsOn variable is in when the drawKeyboard
  // function is called.
  for (i = 0; i < NUMLETTERS; i++) {
    k.letters[i].x_start = button_x;
    k.letters[i].y_start = button_y;
    k.letters[i].width = BUTTON_W;
    k.letters[i].height = BUTTON_H;
    k.letters[i].colour = WHITE;
    k.letters[i].id = i;

    if (i == FIRST_LETTER_ROW_CUTOFF) {
      button_y += BUTTON_H + 10;
      button_x = 90;
    }
    else if ( i == SECOND_LETTER_ROW_CUTOFF ) {
      button_x = 160;
      button_y += BUTTON_H + 10;
    }
    else
      button_x += BUTTON_W + 10;
  }

  button_x = 55;
  button_y = 175;

  // The number and punctuation buttons
  for (i = 0; i < NUMPUNCTNUM; i++) {

	k.numbers_and_punct[i].x_start = button_x;
	k.numbers_and_punct[i].y_start = button_y;
	k.numbers_and_punct[i].width = BUTTON_W;
	k.numbers_and_punct[i].height = BUTTON_H;
	k.numbers_and_punct[i].id = i;
	strcpy(k.numbers_and_punct[i].message, num_punc_str[i]);

	if(i == 25){
		k.numbers_and_punct[i].colour = BLACK;
	} else {
		k.numbers_and_punct[i].colour = WHITE;
	}
    if (i == FIRST_PUNCTNUM_ROW_CUTOFF) {
      button_y += BUTTON_H + 10;
      button_x = 90;
    }
    else if ( i == SECOND_PUNCTNUM_ROW_CUTOFF ) {
      button_x = 160;
      button_y += BUTTON_H + 10;
    }
    else
      button_x += BUTTON_W + 10;
  }

  // The button that toggles whether we are looking at letters or numbers and
  // punctuation (They are the same button, but depending on the state will
  // display either "abc" or "123")
  k.number_letter_toggle.x_start = 90;
  k.number_letter_toggle.y_start = button_y;
  k.number_letter_toggle.width = BUTTON_W;
  k.number_letter_toggle.height = BUTTON_H;
  k.number_letter_toggle.colour = WHITE;
  k.number_letter_toggle.id = 26;

  k.space.x_start = 195;
  k.space.y_start = button_y + BUTTON_H + 10;
  k.space.width = 150;
  k.space.height = BUTTON_H;
  k.space.colour = WHITE;
  k.space.id = 27;
  strcpy(k.space.message, "Space");

  k.enter.x_start = 355;
  k.enter.y_start = button_y + BUTTON_H + 10;
  k.enter.width = 90;
  k.enter.height = BUTTON_H;
  k.enter.colour = WHITE;
  k.enter.id = 28;
  strcpy(k.enter.message, "Enter");

  k.shift.x_start = 455;
  k.shift.y_start = button_y + BUTTON_H + 10;
  k.shift.width = 90;
  k.shift.height = BUTTON_H;
  k.shift.colour = WHITE;
  k.shift.id = 28;
  strcpy(k.shift.message, "Shift");

  k.backspace.x_start = 555;
  k.backspace.y_start = button_y + BUTTON_H + 10;
  k.backspace.width = 50;
  k.backspace.height = BUTTON_H;
  k.backspace.colour = WHITE;
  k.backspace.id = 28;
  strcpy(k.backspace.message, "BS");

  return k;
}

// Function returns 1 of there is something to read from t.last_saved_string.
// Otherwise, this returns 0.
int keyboardListener(Point p, keyboard *k, textbox *t) {
  // Enter key saves the string that is entered in t.last_saved_string and
  // resets t.box.message to nothing. Also closes the keyboard
  if (isButtonPressed(p, k->enter)) {
    k->isKeyboardOn = 0;
    t->isTextboxOn = 0;
    strcpy(t->last_saved_string, t->box.message);
    strcpy(t->box.message, "");
    return 1;
  }
  // Backspace moves the cursor back, and sets the character at the new cursor
  // position to be a null terminator.
  if (isButtonPressed(p, k->backspace)) {
    t->cursor_pos--;
    if (t->cursor_pos < 0) {
      t->cursor_pos = 0;
    }
    k->isCapsOn = 0;

    t->box.message[t->cursor_pos] = '\0';
    return 0;
  }
  else if (isButtonPressed(p, k->space)) {
    strcat(t->box.message, " ");
    t->cursor_pos++;
    k->isCapsOn = 0;
    return 0;
  }
  else if (isButtonPressed(p, k->number_letter_toggle)) {
    if (k->isNumbersOn == 1) {
      k->isNumbersOn = 0;
    }
    else {
      k->isNumbersOn = 1;
    }
    k->isCapsOn = 0;
    return 0;
  }
  else if (isButtonPressed(p, k->shift)) {
    k->isCapsOn = 1;
    return 0;
  }
  else if (k->isNumbersOn) {
    int i;
    for (i = 0; i < NUMPUNCTNUM; i++) {
      if (isButtonPressed(p, k->numbers_and_punct[i])) {
        strcat(t->box.message, num_punc_str[i]);
        t->cursor_pos++;
        k->isCapsOn = 0;
        return 0;
      }
    }
  }
  else {
    int i;
    for (i = 0; i < NUMLETTERS; i++) {
      if (isButtonPressed(p, k->letters[i])) {
        if (k->isCapsOn) {
          strcat(t->box.message, upperletters_str[i]);
        }
        else {
          strcat(t->box.message, lowerletters_str[i]);
        }
        t->cursor_pos++;
        k->isCapsOn = 0;
        return 0;
      }
    }
  }
  return 0;
}

#endif /* KEYBOARD_H_ */
