/*
 * booltype.h
 *
 *  Created on: 2016-02-25
 *      Author: Tanner
 */

#ifndef BOOLTYPE_H_
#define BOOLTYPE_H_

#ifndef bool
    typedef enum e_bool { false = 0, true = 1 } bool;
#endif

#endif /* BOOLTYPE_H_ */
