/*
                                -----------
                           --|1            2|--
                           --|3            4|--
                           --|5            6|--
                           --|7   GPIO 0   8|--
       Purple              --|9    JP1    10|--          White
V+ On Touch Screen -- VCC5 --|11          12|-- GND --- Gnd on Touch Screen
RX On Touch Screen --   Tx --|13          14|-- Rx  --- TX
       Grey                --|15          16|--      Black


*/

#include "GPS.h"
#include "Keyboard.h"

#define B_HEIGHT     20
#define B_WIDTH      20
#define B_COLOR      5 //YELLOW
#define B_SELECTED_COLOUR	7 //MAGENTA
#define NAME_MAX     64


// no building at beginning
int  BUILDING_EXIST=0;

typedef struct {
	char name[NAME_MAX];
	char course[NAME_MAX];
	char latitude[10];
	char longitude[11];
    char address[NAME_MAX];
} building_info;

typedef struct {
	button box;		//physical representation of the building
	button ok;		//button to toggle the building on or off
	textbox info;	//displays user-entered information about class/building
	building_info msg;
} building;

building ubc_buildings[100];

building create_building (touchscreen_coor location){
	button box;
	building ubc_building;
	box.x_start = location.touchscreen_x;
	box.y_start = location.touchscreen_y;
	box.height  = B_HEIGHT;
	box.width = B_WIDTH;
	box.colour = B_COLOR;
	box.has_text = 0;
	drawButton(box);

	ubc_building.box = box;

	return ubc_building;
}

int test_map (int numbers){
	GPS_info test_buildings[5];
	GPS_info math;
	math.E_W_indicator ='W';
	math.N_S_indicator ='N';
	strcpy(math.latitude , "4915.9773");
	strcpy(math.longitude , "12315.3178");
	test_buildings[0] = math;

	GPS_info sauder;
	sauder.E_W_indicator ='W';
	sauder.N_S_indicator ='N';
	strcpy(sauder.latitude , "4915.9157");
	strcpy(sauder.longitude, "12315.2200");
	test_buildings[1] = sauder;

	GPS_info chbe;
	chbe.E_W_indicator ='W';
	chbe.N_S_indicator ='N';
	strcpy(chbe.latitude , "4915.7506");
	strcpy(chbe.longitude, "12314.8312");
	test_buildings[2] = chbe;

	GPS_info totem;
	totem.E_W_indicator ='W';
	totem.N_S_indicator ='N';
	strcpy(totem.latitude , "4915.5716");
	strcpy(totem.longitude, "12315.2412");
	test_buildings[3] = totem;

	GPS_info hospital;
	hospital.E_W_indicator ='W';
	hospital.N_S_indicator ='N';
	strcpy(hospital.latitude,"4915.8497\0");
	strcpy(hospital.longitude, "12314.7500\0");
	test_buildings[4] = hospital;

	int count;
	for(count = 0; count<5; count++){
		 touchscreen_coor onscreen = building_coord(test_buildings[count],B_HEIGHT,B_WIDTH);
		 building build = create_building(onscreen);
		 ubc_buildings[numbers+count]=build;

		 if(count == 0){
			 strcpy(ubc_buildings[count].msg.name,"MATHEMATICAL\0");
			 strcpy(ubc_buildings[count].msg.course, "NO COURSE\0");
			 strcpy(ubc_buildings[count].msg.latitude ,test_buildings[0].latitude);
			 strcpy(ubc_buildings[count].msg.longitude , test_buildings[0].longitude);
			 strcpy(ubc_buildings[count].msg.address,"1984 Mathematics Road, UBC\0");
		 }
	}

	return numbers+5;
}

int main() {
  Point p1;

  Init_Touch();
  Init_GPS();


  ClearScreen(BLACK);

  // Toggle is used to toggle the keyboard on and off.
  button toggle;
  toggle.x_start = 675;
  toggle.y_start = 20;
  toggle.width = 125;
  toggle.height = 80;
  toggle.colour = RED;
  toggle.has_text = 1;
  toggle.id = 1;
  strcpy(toggle.message, "In");
  strcpy(toggle.message2, "Class");

  button map;
  map.x_start = 675;
  map.y_start = 120;
  map.width = 125;
  map.height = 80;
  map.colour = RED;
  map.has_text = 1;
  map.id = 2;
  strcpy(map.message, "Start");
  strcpy(map.message2, "Mapping");

  button load;
  load.x_start = 675;
  load.y_start = 220;
  load.width = 125;
  load.height = 80;
  load.colour = RED;
  load.has_text = 1;
  load.id = 3;
  strcpy(load.message, "Load");
  strcpy(load.message2, "Buildings");

  button back;
  //back.x_start = 675;
  //back.y_start = 400;
  back.width = 125;
  back.height = 80;
  back.colour = RED;
  back.has_text = 1;
  back.id = 4;
  strcpy(back.message, "Back");


  drawButton_two_lines(toggle);
  drawButton_two_lines(map);
  drawButton_two_lines(load);

  keyboard k = initializeKeyboard();
  textbox t = initializeTextbox();

  int numbers = 0;
  int load_is_pressed = 0;
  int in_info_page = 0;

  // Toggle from one menu to another by pressing the new_location button.
  while (1) {
  p1 = GetRelease();
  if(isButtonPressed(p1,load)){
	  BUILDING_EXIST = 1;
	  numbers = test_map(numbers);
	  load_is_pressed = 1;
  }



  else if (isButtonPressed(p1,map)){
	  char* nmea_string = (char*)malloc(sizeof(char));
	       read_one_data_entry (nmea_string);
	       printf("%s\n", nmea_string);
	       while(test_GPGGA(nmea_string) == FALSE){
	           free(nmea_string);
	           char* nmea_string = (char*)malloc(sizeof(char));
	           read_one_data_entry (nmea_string);
	           printf("%s\n", nmea_string);
	       }
	       GPS_info info = parse_GPS_packet (nmea_string);

	  touchscreen_coor onscreen = building_coord(info,B_HEIGHT,B_WIDTH);
	  printf("The x coordinate is:%d and the y coordinate is:%d\n",onscreen.touchscreen_x,onscreen.touchscreen_y);
	  BUILDING_EXIST=1;
	  building build = create_building(onscreen);
	  ubc_buildings[numbers]=build;
	  numbers++;
  }

  else if (isButtonPressed(p1, toggle)) {
    if (k.isKeyboardOn == 1) {
      k.isKeyboardOn = 0;
      t.isTextboxOn = 0;
    }
    else {
      k.isKeyboardOn = 1;
      t.isTextboxOn = 1;
    }
  }
  else if (k.isKeyboardOn) {
    int isThereSomethingToRead = 0;
    // checks if the touch was on a keyboard key. If it was, it preforms the
    // action of that key.
    isThereSomethingToRead = keyboardListener(p1, &k, &t);

    if (isThereSomethingToRead) {
      printf("t.last_string contains string: %s\n", t.last_saved_string);
    }
  }

     if(!k.isKeyboardOn){
    	 ClearScreen(BLACK);
    	 drawButton_two_lines(map);
    	 drawButton_two_lines(toggle);
    	 drawButton_two_lines(load);
    	 if(BUILDING_EXIST){
    		 int count;
    		 for(count = 0; count <= numbers; count++){
    			 drawButton(ubc_buildings[count].box);
    		 }
    	 }
    	 if(load_is_pressed){
    	     	  if(isButtonPressed(p1,ubc_buildings[0].box) && !k.isKeyboardOn){
    	     	  		  //ClearScreen(WHITE);
						  FramedRectangle(ubc_buildings[0].x_start, ubc_buildings[0].y_start, B_WIDTH, B_HEIGHT, B_SELECTED_COLOUR, B_SELECTED_COLOUR);
						  if( ubc_buildings[0].x_start < (200-B_WIDTH/2) ){
							  if( ubc_buildings[0].y_start > 230 )
								FramedRectangle((ubc_buildings[0].x_start + B_WIDTH + 10), ubc_buildings[0].y_start, 150,150,WHITE,WHITE);
							  else
								 FramedRectangle((ubc_buildings[0].x_start + B_WIDTH + 10), ubc_buildings[0].y_start + 150, 150,150,WHITE,WHITE);
							  printString(ubc_buildings[0].msg.name, ubc_buildings[0].x_start + B_WIDTH + 15, ubc_buildings[0].y_start - 5, BLACK, CYAN);
							  printString(ubc_buildings[0].msg.course, ubc_buildings[0].x_start + B_WIDTH + 15, ubc_buildings[0].y_start - 15 ,BLACK, CYAN);
							  printString(ubc_buildings[0].msg.latitude, ubc_buildings[0].x_start + B_WIDTH + 15, ubc_buildings[0].y_start - 25 ,BLACK, CYAN);
							  printString(ubc_buildings[0].msg.longitude, ubc_buildings[0].x_start + B_WIDTH + 15, ubc_buildings[0].y_start - 35,BLACK, CYAN);
							  printString(ubc_buildings[0].msg.address, ubc_buildings[0].x_start + B_WIDTH + 15, ubc_buildings[0].y_start - 45,BLACK, CYAN);

						  } else {
							  if( ubc_buildings[0].y_start > 230 )
								FramedRectangle((ubc_buildings[0].x_start + B_WIDTH + 10), ubc_buildings[0].y_start, 150,150,WHITE,WHITE);
							  else
								 FramedRectangle((ubc_buildings[0].x_start + B_WIDTH + 10), ubc_buildings[0].y_start + 150, 150,150,WHITE,WHITE);
							  printString(ubc_buildings[0].msg.name, ubc_buildings[0].x_start - 5, ubc_buildings[0].y_start - 5, BLACK, CYAN);
							  printString(ubc_buildings[0].msg.course, ubc_buildings[0].x_start - 5, ubc_buildings[0].y_start - 15, BLACK, CYAN);
							  printString(ubc_buildings[0].msg.latitude, ubc_buildings[0].x_start - 5, ubc_buildings[0].y_start - 25, BLACK, CYAN);
							  printString(ubc_buildings[0].msg.longitude, ubc_buildings[0].x_start - 5, ubc_buildings[0].y_start - 35, BLACK, CYAN);
							  printString(ubc_buildings[0].msg.address, ubc_buildings[0].x_start - 5, ubc_buildings[0].y_start - 45, BLACK, CYAN);
							  back.y_start = 400;
						  }
						  back.y_start = ubc_buildings[0].y_start - 150;
						  back.x_start = ubc_buildings[0].x_start + 12;
    	     	  		  drawButton(back);	
    	     	  		  in_info_page = 1;
    	     	  }
    	       }

    	       if(in_info_page){
    	     	  if(isButtonPressed(p1,back)){
    	     		  in_info_page = 0;
    	     	  	  			  ClearScreen(BLACK);
    	     	  	  			      	 drawButton_two_lines(map);
    	     	  	  			      	 drawButton_two_lines(toggle);
    	     	  	  			      	 drawButton_two_lines(load);
    	     	  	  			      	 if(BUILDING_EXIST){
    	     	  	  			      		 int count;
    	     	  	  			      		 for(count = 0; count <= numbers; count++){
    	     	  	  			      			 drawButton(ubc_buildings[count].box);
    	     	  	  			      		 }
    	     	  	  			      	 }
    	     	  	  		  }
    	       }
			   
     }

     else{
    	 ClearScreen(BLACK);
    	 drawButton_two_lines(toggle);
    	 drawKeyboard(&k);
    	 drawTextbox(t);
     }


    wait_xk_loops(200);
  }

  return 0;

}
