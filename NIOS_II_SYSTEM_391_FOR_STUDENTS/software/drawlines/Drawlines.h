/*
 * Drawlines.h
 *
 *  Created on: 2016-02-24
 *      Author: k8w9a
 */

#ifndef DRAWLINES_H_
#define DRAWLINES_H_

// graphics registers all address begin with '8' so as to by pass data cache on NIOS

#define  GraphicsCommandReg           (*(volatile unsigned short int *)(0x84000000))
#define  GraphicsStatusReg            (*(volatile unsigned short int *)(0x84000000))
#define  GraphicsX1Reg                (*(volatile unsigned short int *)(0x84000002))
#define  GraphicsY1Reg                (*(volatile unsigned short int *)(0x84000004))
#define  GraphicsX2Reg                (*(volatile unsigned short int *)(0x84000006))
#define  GraphicsY2Reg                (*(volatile unsigned short int *)(0x84000008))
#define  GraphicsColourReg            (*(volatile unsigned short int *)(0x8400000E))
#define  GraphicsBackGroundColourReg  (*(volatile unsigned short int *)(0x84000010))

/************************************************************************************************
 ** This macro pauses until the graphics chip status register indicates that it is idle
 ***********************************************************************************************/

#define WAIT_FOR_GRAPHICS    while ((GraphicsStatusReg & 0x0001) != 0x0001);

//Here are some software routines to drive some simple graphics functions

// #defined constants representing values we write to the graphics 'command' register to get
// it to draw something. You will add more values as you add hardware to the graphics chip
// Note DrawHLine, DrawVLine and DrawLine at the moment do nothing - you will modify these

#define DrawHLine             1
#define DrawVLine             2
#define DrawLine              3
#define PutAPixel             0xA
#define GetAPixel             0xB
#define ProgramPaletteColour  0x10

// defined constants representing colours pre-programmed into colour palette
// there are 256 colours but only 8 are shown below, we write these to the colour registers
//
// the header files "Colours.h" contains constants for all 256 colours
// while the course file ColourPaletteData.c contains the 24 bit RGB data
// that is pre-programmed into the palette

#define BLACK    0
#define WHITE    1
#define RED      2
#define LIME     3
#define BLUE     4
#define YELLOW   5
#define CYAN     6
#define MAGENTA  7

#define push_buttons123 (volatile char *) 0x8001060

/*******************************************************************************************
 * This function writes a single pixel to the x,y coords specified using the specified colour
 * Note colour is a byte and represents a palette number (0-255) not a 24 bit RGB value
 ********************************************************************************************/
void WriteAPixel(int x, int y, int Colour) {
    WAIT_FOR_GRAPHICS;              // is graphics ready for new command

    GraphicsX1Reg = x;              // write coords to x1, y1
    GraphicsY1Reg = y;

    GraphicsColourReg = Colour;     // set pixel colour
    GraphicsCommandReg = PutAPixel; // give graphics "write pixel" command
}

/*********************************************************************************************
 * This function read a single pixel from the x,y coords specified and returns its colour
 * Note returned colour is a byte and represents a palette number (0-255) not a 24 bit RGB value
 *********************************************************************************************/

int ReadAPixel(int x, int y) {
    WAIT_FOR_GRAPHICS;                // is graphics ready for new command

    GraphicsX1Reg = x;                // write coords to x1, y1
    GraphicsY1Reg = y;

    GraphicsCommandReg = GetAPixel;   // give graphics a "get pixel" command

    WAIT_FOR_GRAPHICS;                // is graphics done reading pixel
    return (int)(GraphicsColourReg);  // return the palette number (colour)
}


/**********************************************************************************
 ** subroutine to program a hardware (graphics chip) palette number with an RGB value
 ** e.g. ProgramPalette(RED, 0x00FF0000) ;
 **
 ************************************************************************************/

void ProgramPalette(int PaletteNumber, int RGB) {
    WAIT_FOR_GRAPHICS;
    GraphicsColourReg  = PaletteNumber;
    GraphicsX1Reg      = RGB >> 16;              // program red value in ls.8 bit of X1 reg
    GraphicsY1Reg      = RGB;                    // program green and blue into ls 16 bit of Y1 reg
    GraphicsCommandReg = ProgramPaletteColour;   // issue command
}

/*********************************************************************************************
 This function draw a horizontal line, 1 pixel at a time starting at the x,y coords specified
 *********************************************************************************************/

void HLine(int x1, int y1, int length, int Colour) {
    WAIT_FOR_GRAPHICS;
    GraphicsX1Reg = x1;
    GraphicsY1Reg = y1;
    GraphicsX2Reg = x1 + length;
    GraphicsColourReg = Colour;
    GraphicsCommandReg = DrawHLine;
}

/*********************************************************************************************
 This function draw a vertical line, 1 pixel at a time starting at the x,y coords specified
 *********************************************************************************************/

void VLine(int x1, int y1, int length, int Colour) {
    WAIT_FOR_GRAPHICS;
    GraphicsX1Reg = x1;
    GraphicsY1Reg = y1;
    GraphicsY2Reg = y1 + length;
    GraphicsColourReg = Colour;
    GraphicsCommandReg = DrawVLine;
}

/*******************************************************************************
 ** Implementation of Bresenhams line drawing algorithm
 *******************************************************************************/

void FramedRectangle(int x1, int y1, int width, int height, int framecolour, int fillcolour ) {

    HLine(x1, y1, width, framecolour);
    int tempy1;

    for (tempy1 = y1+1; tempy1 < y1+height; tempy1++) {
        HLine(x1+1, tempy1, width-1,fillcolour);
    }

    HLine(x1, y1+height, width, framecolour);
    VLine(x1, y1, height-1, framecolour);
    VLine(x1 + width, y1, height-1, framecolour);

}


void Line(int x1, int y1, int x2, int y2, int Colour) {
    WAIT_FOR_GRAPHICS;
    GraphicsX1Reg = x1;
    GraphicsY1Reg = y1;
    GraphicsX1Reg = x2;
    GraphicsY2Reg = y2;
    GraphicsColourReg = Colour;
    GraphicsCommandReg = DrawLine;
}


void ClearScreen(int colour) {
    FramedRectangle(0, 0, 800, 480, colour, colour);
}


#endif /* DRAWLINES_H_ */
