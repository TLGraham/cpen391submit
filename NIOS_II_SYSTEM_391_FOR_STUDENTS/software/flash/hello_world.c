#include <stdio.h>
#include <altera_up_sd_card_avalon_interface.h>

#define NULL 0
int SDconnect(void) {
	alt_up_sd_card_dev *device_reference = NULL;
	int connected = 0;
	printf("Opening SDCard\n");
	if ((device_reference = alt_up_sd_card_open_dev(
			"/dev/Altera_UP_SD_Card_Avalon_Interface_0")) == NULL) {
		printf("SDCard Open FAILED\n");
		return 0;
	} else
		printf("SDCard Open PASSED\n");
	if (device_reference != NULL) {
		while (1) {
			if ((connected == 0) && (alt_up_sd_card_is_Present())) {
				printf("Card connected.\n");
				if (alt_up_sd_card_is_FAT16()) {
					printf("FAT16 file system detected.\n");
					goto done;
				} else {
					printf("Unknown file system.\n");
					return -1;
				}
				connected = 1;
			} else if ((connected == 1)
					&& (alt_up_sd_card_is_Present() == false)) {
				printf("Card disconnected.\n");
				connected = 0;
				return -2;
			}
		}
	} else
		printf("Can't open device\n");
	return -3;
	done: return 1;
}

short int getBuildingCount(void) {
	short int fd;
	short int numBuildings;
	short int error;
	if (alt_up_sd_card_is_Present() && alt_up_sd_card_is_FAT16()) {
		if ((fd = alt_up_sd_card_fopen("build.txt", false)) != -1) {
			numBuildings = alt_up_sd_card_read(fd);
		} else {
			error = alt_up_sd_card_fclose(fd);
			return -5;
		}
	}
	error = alt_up_sd_card_fclose(fd);
	if (error < 0) return error;
	return numBuildings;
}
int setBuildingCount( short int numBuildings) {
	short int fd;
	int error;
	if (alt_up_sd_card_is_Present() && alt_up_sd_card_is_FAT16()) {
		if ((fd = alt_up_sd_card_fopen("build.txt", false)) != -1) {
			error = alt_up_sd_card_write(fd, numBuildings);
		} else {
			error = alt_up_sd_card_fclose(fd);
			return -5;
		}
	}
	error = alt_up_sd_card_fclose(fd);
	if(error < 0) return error;
	return numBuildings;
}

int main(){
	int error;
	short int numbuildings = 15;
	error = SDconnect();
	if(error < 0){
		printf("error opening SD card\n");
		return 0;
	}
	error = setBuildingCount(numbuildings);
	if(error < 0){
		printf("error setting number of buildings: %d\n", error);
		return 0;
	}
	error = getBuildingCount();
	if(error < 0){
		printf("error getting number of buildings: %d\n", error);
		return 0;
	}
	if(error != numbuildings) printf("Number of buildings does not match");
	printf("%d", error);
}
